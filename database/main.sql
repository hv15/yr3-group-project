-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 27, 2013 at 09:40 PM
-- Server version: 5.5.29
-- PHP Version: 5.3.10-1ubuntu3.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `HWSports`
--
CREATE DATABASE `HWSports` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `HWSports`;

-- --------------------------------------------------------

--
-- Table structure for table `Competitors`
--

CREATE TABLE IF NOT EXISTS `Competitors` (
  `username` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `firstname` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `dob` date NOT NULL,
  `about` text COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `telephone` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `sport` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `team` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- RELATIONS FOR TABLE `Competitors`:
--   `sport`
--       `Sports` -> `sport`
--   `team`
--       `Teams` -> `username`
--   `username`
--       `Users` -> `username`
--

-- --------------------------------------------------------

--
-- Table structure for table `Matches`
--

CREATE TABLE IF NOT EXISTS `Matches` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  `sport` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `umpire` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `location` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- RELATIONS FOR TABLE `Matches`:
--   `sport`
--       `Sports` -> `sport`
--

-- --------------------------------------------------------

--
-- Table structure for table `Schedules`
--

CREATE TABLE IF NOT EXISTS `Schedules` (
  `match` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `member` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- RELATIONS FOR TABLE `Schedules`:
--   `match`
--       `Matches` -> `title`
--

-- --------------------------------------------------------

--
-- Table structure for table `Scores`
--

CREATE TABLE IF NOT EXISTS `Scores` (
  `match` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `member` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `for` int(3) DEFAULT NULL,
  `against` int(3) DEFAULT NULL,
  `time` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Soldtickets`
--

CREATE TABLE IF NOT EXISTS `Soldtickets` (
  `num` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `firstname` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastname` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `telephone` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Sports`
--

CREATE TABLE IF NOT EXISTS `Sports` (
  `sport` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`sport`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Sports`
--

INSERT INTO `Sports` (`sport`) VALUES
('hurdling'),
('wattball');

-- --------------------------------------------------------

--
-- Table structure for table `Staff`
--

CREATE TABLE IF NOT EXISTS `Staff` (
  `username` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `firstname` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `role` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `telephone` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `admin` bit(1) DEFAULT NULL,
  UNIQUE KEY `username` (`username`),
  KEY `role` (`role`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- RELATIONS FOR TABLE `Staff`:
--   `username`
--       `Users` -> `username`
--

-- --------------------------------------------------------

--
-- Table structure for table `Teams`
--

CREATE TABLE IF NOT EXISTS `Teams` (
  `username` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `team` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `about` text COLLATE utf8_unicode_ci NOT NULL,
  `coachfname` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `coachlname` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `telephone` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `membernum` int(2) NOT NULL,
  `membership` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `membershipnum` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Tickets`
--

CREATE TABLE IF NOT EXISTS `Tickets` (
  `date` date NOT NULL,
  `available` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Users`
--

CREATE TABLE IF NOT EXISTS `Users` (
  `username` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `password` char(128) COLLATE utf8_unicode_ci NOT NULL,
  `role` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `hidden` bit(1) DEFAULT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Users`
--

INSERT INTO `Users` (`username`, `password`, `role`, `hidden`) VALUES
('admin', '82968693c8945aaab42affa946479c78a7fffc0513e7b7f741c8d91baa3a74517f8ffab88c6ff5f914b3d7a931a59f73c7762dcd079824ed24e75037d016c7ec', 'staff', NULL);
