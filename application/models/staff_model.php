<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Staff_model extends CI_Model {

	const TABLE = 'Staff';
	const IDFIELD = 'username';

	function __construct(){
		parent::__construct();
	}
	
	function order($sort_f = NULL, $sort_d = NULL)
	{
		if(!empty($sort_f) && !empty($sort_d))
		{
			$this->db->order_by($sort_f, $sort_d);
		}
	}
	
	function or_like($find_f = NULL, $find_m = NULL)
	{
		if(!empty($find_f) && !empty($find_m))
		{
			$this->db->or_like($find_f, $find_m);
		}
	}
	
	function like($find_f = NULL, $find_m = NULL)
	{
		if(!empty($find_f) && !empty($find_m))
		{
			$this->db->like($find_f, $find_m);
		}
	}

	function get_table($select = '*', $limit = NULL, $end = NULL)
	{
		$this->db->select($select);
		if(!empty($limit) && !empty($end))
		{
			$this->db->limit($limit,$end);
		}
		else if(!empty($limit) && empty($end))
		{
			$this->db->limit($limit);
		}
		return $this->db->get(self::TABLE)->result_array();
	}
	
	function get_all_count()
	{
		return $this->db->count_all(self::TABLE);
	}
	
	function get_result_count()
	{
		return $this->db->count_all_results(self::TABLE);
	}

	function get_details($username)
	{
		$query = $this->db->get_where(self::TABLE, array('username' => $username), 1);
		return $query->result_array();
	}

	function find_details($pattern, $case = FALSE)
	{
		$table = $this->get_table();
		return $this->arrays->search_in_array($pattern, $table, $case);
	}
	
	function change_details($key, $column, $value)
	{
		$value = ($value == 'NULL' ? NULL : ($value == '1' ? 1 : $value));
		$this->db->update(self::TABLE, array($column => $value), array(self::IDFIELD => $key));
		if($this->db->affected_rows() > 0)
			return TRUE;
		else
			return FALSE;
	}
	
	function remove_details($key)
	{
		$this->db->delete(self::TABLE, array(self::IDFIELD => $key));
		if($this->db->affected_rows() > 0)
			return TRUE;
		else
			return FALSE;
	}

	function add_details($username, $firstname, $lastname, $role, $address, $telephone, $email, $is_admin)
	{
		$data = array(
			'username' => $username,
			'firstname' => $firstname,
			'lastname' => $lastname,
			'role' => $role,
			'address' => $address,
			'telephone' => $telephone,
			'email' => $email,
			'admin' => (empty($is_admin) ? NULL : 1)
		);
		return $this->db->insert(self::TABLE, $data);
	}
}	

/* End of file staff_model.php */
/* Location: ./application/models/staff_model.php */
