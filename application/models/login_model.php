<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login_model extends CI_Model {

	const TABLE = 'Users';
	const IDFIELD = 'username';

	function __construct(){
		parent::__construct();
	}

	function order($sort_f = NULL, $sort_d = NULL)
	{
		if(!empty($sort_f) && !empty($sort_d))
		{
			$this->db->order_by($sort_f, $sort_d);
		}
	}
	
	function or_like($find_f = NULL, $find_m = NULL)
	{
		if(!empty($find_f) && !empty($find_m))
		{
			$this->db->or_like($find_f, $find_m);
		}
	}
	
	function like($find_f, $find_m)
	{
		$this->db->like($find_f, $find_m);
	}
	
	function get_table($select = '*', $limit = NULL, $end = NULL, $hash = FALSE)
	{
		$select = 'username,role,hidden';
		if($hash)
			$select = '*';
		$this->db->select($select);
		if(!empty($limit) && !empty($end))
		{
			$this->db->limit($limit,$end);
		}
		else if(!empty($limit) && empty($end))
		{
			$this->db->limit($limit);
		}
		return $this->db->get(self::TABLE)->result_array();
	}
	
	function get_all_count()
	{
		return $this->db->count_all(self::TABLE);
	}
	
	function get_result_count()
	{
		return $this->db->count_all_results(self::TABLE);
	}

	function get_details($username, $get_pass = FALSE){
		$select = 'username,role,hidden';
		if($get_pass)
			$select = '*';
		$this->db->select($select);
		$query = $this->db->get_where(self::TABLE, array(self::IDFIELD => $username), 1);
		return $query->result_array();
	}
	
	function get_by_role($role, $hidden = FALSE){
		if($hidden)
		{
			$select = 'username,hidden';
			$where = array('role' => $role);
		} 
		else 
		{
			$select = 'username,hidden';
			$where = array('role' => $role, 'hidden' => 1);
		}
		$this->db->select($select);
		$query = $this->db->get_where(self::TABLE, $where);
		return $query->result_array();
	}

	function find_details($pattern, $case = FALSE, $get_pass = FALSE){
		$select = 'username,role,hidden';
		if($get_pass)
			$select = '*';
		$table = $this->get_table($select);
		return $this->arrays->search_in_array($pattern, $table, $case);
	}

	function add_details($username, $password, $role, $hidden = NULL){
		if(empty($username) || empty($password) || empty($role))
			return FALSE;
		$salt = $this->password->hash_salt($username);
		$data = array(
			'username' => $username,
			'password' => $this->password->hash_pass($password,$salt),
			'role' => $role,
			'hidden' => $hidden
		);
		return $this->db->insert(self::TABLE, $data);
	}
	
	function change_details($key, $column, $value)
	{
		if($column == 'password')
			return FALSE;

		$value = ($value == 'NULL' ? NULL : ($value == '1' ? 1 : $value));
		$this->db->update(self::TABLE, array($column => $value), array(self::IDFIELD => $key));
		if($this->db->affected_rows() > 0)
			return TRUE;
		else
			return FALSE;
	}
	
	function change_password($username, $password)
	{
		$salt = $this->password->hash_salt($username);
		$this->db->update(self::TABLE, array('password' => $this->password->hash_pass($password,$salt)), array(self::IDFIELD => $username));
		if($this->db->affected_rows() > 0)
			return TRUE;
		else
			return FALSE;
	}
	
	function remove_details($key)
	{
		$this->db->delete(self::TABLE, array(self::IDFIELD => $key));
		if($this->db->affected_rows() > 0)
			return TRUE;
		else
			return FALSE;
	}

	function user_exist($username){
		if(empty($username))
			return FALSE;
		$table = $this->get_details($username);
		if(!empty($table))
			return TRUE;
		return FALSE;
	}

	function verify_user($username, $passhash){
		$user = $this->get_details($username,TRUE);
		$salt = substr($user['password'], 0, 64);
		$hash = $this->password->hash_pass($passhash, $salt);
		if($hash == $user['password'])
			return TRUE;
		return FALSE;	
	}
}	

/* End of file login_model.php */
/* Location: ./application/models/login_model.php */
