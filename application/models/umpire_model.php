<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Umpire_model extends CI_Model {

	const TABLE = 'Umpires';

	function __construct(){
		parent::__construct();
	}
	
	function order($sort_f = NULL, $sort_d = NULL)
	{
		if(!empty($sort_f) && !empty($sort_d))
		{
			$this->db->order_by($sort_f, $sort_d);
		}
	}
	
	function or_like($find_f = NULL, $find_m = NULL)
	{
		if(!empty($find_f) && !empty($find_m))
		{
			$this->db->or_like($find_f, $find_m);
		}
	}
	
	function like($find_f = NULL, $find_m = NULL)
	{
		if(!empty($find_f) && !empty($find_m))
		{
			$this->db->like($find_f, $find_m);
		}
	}
	
	function get_all_count()
	{
		return $this->db->count_all(self::TABLE);
	}
	
	function get_result_count()
	{
		return $this->db->count_all_results(self::TABLE);
	}

	function get_umpire_table($select = '*'){
		$this->db->select($select);
		$query = $this->db->get(self::TABLE);
		return $query->result_array();
	}

	function get_umpire_details($username){
		$select = '*';
		$this->db->select($select);
		$query = $this->db->get_where(self::TABLE, array('username' => $username), 1);
		return $query->result_array();
	}

	function find_umpire_details($pattern, $case = FALSE){
		$select = '*';
		$table = $this->get_user_table($select);
		return $this->arrays->search_in_array($pattern, $table, $case);
	}

	function add_umpire_details($username, $firstname, $lastname, $address, $phone, $email){
		$data = array(
			'username' => $username,
			'firstname' => $firstname,
			'lastname' => $lastname,
			'address' => $address,
			'telephone' => $phone,
			'email' => $email
		);
		return $this->db->insert(self::TABLE, $data);
	}
}	

/* End of file umpire_model.php */
/* Location: ./application/models/umpire_model.php */
