<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sport_model extends CI_Model {

	const TABLE = 'Sports';
	const IDFIELD = 'sport';

	function __construct(){
		parent::__construct();
	}
	
	function get_all_count()
	{
		return $this->db->count_all(self::TABLE);
	}
	
	function get_result_count()
	{
		return $this->db->count_all_results(self::TABLE);
	}
	
	function order($sort_f = NULL, $sort_d = NULL)
	{
		if(!empty($sort_f) && !empty($sort_d))
		{
			$this->db->order_by($sort_f, $sort_d);
		}
	}
	
	function or_like($find_f = NULL, $find_m = NULL)
	{
		if(!empty($find_f) && !empty($find_m))
		{
			$this->db->or_like($find_f, $find_m);
		}
	}
	
	function like($find_f = NULL, $find_m = NULL)
	{
		if(!empty($find_f) && !empty($find_m))
		{
			$this->db->like($find_f, $find_m);
		}
	}

	function get_table($select = '*'){
		$this->db->select($select);
		$query = $this->db->get(self::TABLE);
		return $query->result_array();
	}

	function find_details($pattern, $case = FALSE){
		$select = '*';
		$table = $this->get_table($select);
		return $this->arrays->search_in_array($pattern, $table, $case);
	}

	function add_details($sport){
		$data = array('sport' => $sport);
		return $this->db->insert(self::TABLE, $data);
	}
	
	function change_details($key, $column, $value)
	{
		$value = ($value == 'NULL' ? NULL : ($value == '1' ? 1 : $value));
		$this->db->update(self::TABLE, array($column => $value), array(self::IDFIELD => $key));
		if($this->db->affected_rows() > 0)
			return TRUE;
		else
			return FALSE;
	}
	
	function remove_details($key)
	{
		$this->db->delete(self::TABLE, array(self::IDFIELD => $key));
		if($this->db->affected_rows() > 0)
			return TRUE;
		else
			return FALSE;
	}

	function is_sport($pattern, $return = FALSE){
		$result = $this->find_details($pattern);
		if($return)
			return $result;
		else
			return !empty($result);
	}
}	

/* End of file sport_model.php */
/* Location: ./application/models/sport_model.php */
