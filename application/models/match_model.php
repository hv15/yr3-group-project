<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Match_model extends CI_Model {

	const TABLE = 'Matches';
	const IDFIELD = 'id';

	function __construct(){
		parent::__construct();
	}
	
	function order($sort_f = NULL, $sort_d = NULL)
	{
		if(!empty($sort_f) && !empty($sort_d))
		{
			$this->db->order_by($sort_f, $sort_d);
		}
	}
	
	function or_like($find_f = NULL, $find_m = NULL)
	{
		if(!empty($find_f) && !empty($find_m))
		{
			$this->db->or_like($find_f, $find_m);
		}
	}
	
	function like($find_f = NULL, $find_m = NULL)
	{
		if(!empty($find_f) && !empty($find_m))
		{
			$this->db->like($find_f, $find_m);
		}
	}
	
	function get_all_count()
	{
		return $this->db->count_all(self::TABLE);
	}
	
	function get_result_count()
	{
		return $this->db->count_all_results(self::TABLE);
	}

	function get_table($select = '*'){
		$this->db->select($select);
		$query = $this->db->get(self::TABLE);
		return $query->result_array();
	}
	
	function get_by_interval($start = NULL, $end = NULL){
		if(empty($start) || empty($end))
			return $this->get_table();
		$this->db->where('start >=', date('Y-m-d H:i:s',$start));
		$this->db->where('end <=', date('Y-m-d H:i:s',$end));
		$query = $this->db->get(self::TABLE);
		return $query->result_array();
	}

	function find_details($pattern, $case = FALSE){
		$select = '*';
		$table = $this->get_user_table($select);
		return $this->arrays->search_in_array($pattern, $table, $case);
	}

	function add_details($match, $start_time, $end_time, $sport, $umpire_un, $location){
		$data = array(
			'title' => $match,
			'start' => $start_time,
			'end' => $end_time,
			'sport' => $sport,
			'umpire' => $umpire_un,
			'location' => $location
		);
		return $this->db->insert(self::TABLE, $data);
	}
	
	function change_details($key, $column, $value)
	{
		$value = ($value == 'NULL' ? NULL : ($value == '1' ? 1 : $value));
		$this->db->update(self::TABLE, array($column => $value), array(self::IDFIELD => $key));
		if($this->db->affected_rows() > 0)
			return TRUE;
		else
			return FALSE;
	}
	
	function remove_details($key)
	{
		$this->db->delete(self::TABLE, array(self::IDFIELD => $key));
		if($this->db->affected_rows() > 0)
			return TRUE;
		else
			return FALSE;
	}

	function is_match($pattern, $return = FALSE){
		$result = $this->find_match_details($pattern);
		if($return)
			return $result;
		else
			return !empty($result);
	}
}	

/* End of file match_model.php */
/* Location: ./application/models/match_model.php */
