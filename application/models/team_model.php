<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Team_model extends CI_Model {

	const TABLE = 'Teams';
	const IDFIELD = 'username';

	function __construct(){
		parent::__construct();
	}
	
	function order($sort_f = NULL, $sort_d = NULL)
	{
		if(!empty($sort_f) && !empty($sort_d))
		{
			$this->db->order_by($sort_f, $sort_d);
		}
	}
	
	function or_like($find_f = NULL, $find_m = NULL)
	{
		if(!empty($find_f) && !empty($find_m))
		{
			$this->db->or_like($find_f, $find_m);
		}
	}
	
	function like($find_f = NULL, $find_m = NULL)
	{
		if(!empty($find_f) && !empty($find_m))
		{
			$this->db->like($find_f, $find_m);
		}
	}
	
	function get_all_count()
	{
		return $this->db->count_all(self::TABLE);
	}
	
	function get_result_count()
	{
		return $this->db->count_all_results(self::TABLE);
	}

	function get_table($select = '*'){
		$this->db->select($select);
		$query = $this->db->get(self::TABLE);
		return $query->result_array();
	}

	function get_details($username, $name = FALSE){
		$where = array('username' => $username);
		if($name)
			$where = array('team' => $username);
		$query = $this->db->get_where(self::TABLE, $where, 1);
		return $query->result_array();
	}

	function find_details($pattern, $case = FALSE){
		$select = '*';
		$table = $this->get_user_table($select);
		return $this->arrays->search_in_array($pattern, $table, $case);
	}

	function add_details($username, $team, $fcoach, $lcoach, $address, $telephone, $email, $num, $mem, $memnum, $about = ''){
		$data = array(
			'username' => $username,
			'team' => $team,
			'coachfname' => $fcoach,
			'coachlname' => $lcoach,
			'address' => $address,
			'telephone' => $telephone,
			'email' => $email,
			'membernum' => $num,
			'membership' => (empty($mem) ? NULL : $mem),
			'membershipnum' => (empty($memnum) ? NULL : $memnum),
			'about' => $about
		);
		return $this->db->insert(self::TABLE, $data);
	}
	
	function change_details($key, $column, $value)
	{
		$this->db->update(self::TABLE, array($column => (($value == 'NULL' || empty($value)) ? NULL : $value)), array(self::IDFIELD => $key));
		if($this->db->affected_rows() > 0)
			return TRUE;
		else
			return FALSE;
	}
	
	function remove_details($key)
	{
		$this->db->delete(self::TABLE, array(self::IDFIELD => $key));
		if($this->db->affected_rows() > 0)
			return TRUE;
		else
			return FALSE;
	}
}	

/* End of file team_model.php */
/* Location: ./application/models/team_model.php */
