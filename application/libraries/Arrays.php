<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Arrays {

	function search_in_array($needle, $haystack, $case = FALSE, $limit = 0){

		$result = array();
		$count = 0;
		$search = $haystack;

	//	if($case){
	//		$search = $this->array_map_deep($haystack, 'strtolower');
	//		$needle = strtolower($needle);
	//	}

		foreach($search as $key => $array){
			if($count > 0 && $count == $limit)
				return $result;
			if($this->in_array_match($needle, $array, $case))
				array_push($result,$haystack[$key]);
			$count++;
		}

		return $result;
	}

	function array_map_deep($array, $callback) {

		$new = array();

		if(is_array($array)){
			foreach ($array as $key => $val) {
				if (is_array($val)) {
					$new[$key] = $this->array_map_deep($val, $callback);
				} else
					$new[$key] = call_user_func($callback, $val);
			}
		} else
			$new = call_user_func($callback, $array);

		return $new;
	}

	function in_array_match($pattern, $array, $case = FALSE){
		
		if($case){
			foreach($array as $value){
				if(stripos($value, $pattern) !== FALSE)
					return TRUE;
			}
		} else {
			foreach($array as $value){
				if(strpos($value, $pattern) !== FALSE)
					return TRUE;
			}
			
		}
		return FALSE;
	}
}

/* End of file Array.php */
/* Location: ./application/libraries/Array.php */
