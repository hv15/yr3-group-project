<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Userdetails {
	
	private $CI;
	
	function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->library('session');
	}
	
	function user_session($title, $user = NULL)
	{
		return array(
			'title' => $title,
			'user' => empty($user) ? $this->CI->session->userdata('user') : $user,
			'admin' => $this->CI->session->userdata('admin'),
			'logged' => $this->CI->session->userdata('logged')
		);
	}
	
	function get_details($username, $role)
	{
		$models = array();
		switch($role)
		{
			case 'staff':
				$this->CI->load->model('staff_model','',TRUE);
				$models[$role] = $this->CI->staff_model;
				break;
			case 'umpire':
				return array();
				break;
			case 'competitor':
				$this->CI->load->model('competitor_model','',TRUE);
				$models[$role] = $this->CI->competitor_model;
				break;
			case 'team':
				$this->CI->load->model('team_model','',TRUE);
				$models[$role] = $this->CI->team_model;
				break;
			default:
				return array();
				break;
		}
		$return = $models[$role]->get_details($username);
		return $return;
	}
	
	function sports_array()
	{
		$this->CI->load->model('sport_model','sport',TRUE);
		$sports = $this->CI->sport->get_table();
		$data = array();
		foreach($sports as $sport)
		{
			array_push($data, $sport['sport']);
		}
		return $data;
	}
	
	function teams_array()
	{
		$this->CI->load->model('team_model','team',TRUE);
		$teams = $this->CI->team->get_table();
		$data = array();
		foreach($teams as $team)
		{
			array_push($data, $team['team']);
		}
		return $data;
	}
	
	function team_name($username)
	{
		$team = $this->get_details($username, 'team');
		return $team[0]['team'];
	}
	
	function team_member_array($username, $hidden = FALSE)
	{
		$this->CI->load->model('competitor_model','competitor',TRUE);
		$this->CI->load->model('login_model','login',TRUE);
		$comp_users = $this->CI->login->get_by_role('competitor', $hidden);
		$users = array();
		foreach($comp_users as $key=>$user)
		{
			$users[$key] = $user['username'];
		}
		if(empty($users))
			return array();
		$this->CI->db->where(array('team' => $username));
		$this->CI->db->where_in('username', $users);
		$comps = $this->CI->competitor->get_table('username,firstname,lastname');
		return $comps;
	}
	
	function details_array($role, $hidden = FALSE)
	{
		$return = array();
		$this->CI->load->model('login_model','login',TRUE);
		$users = $this->CI->login->get_by_role($role, $hidden);
		foreach($users as $user)
		{
			$inter = $this->get_details($user['username'], $role);
			//echo $user['username'];
			//print_r($inter);
			array_push($return, $inter[0]);
		}
		return $return;
	}
}

/* End of file Userdetails.php */
/* Location: ./application/libraries/Userdetails.php */