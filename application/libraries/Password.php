<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Password {

	function hash_pass($password, $salt){
		$hash = $salt . $password;
		for($i = 0; $i < 100000; $i++)
			$hash = hash('sha256', $hash);
		return $salt . $hash;
	}

	function hash_salt($username){
		return hash('sha256', uniqid(mt_rand(), TRUE) . 'W1nt3r' . strtolower($username));
	}

	function is_valid_pass($hash, $password){
		$salt = substr($hash, 0, 64);
		$test = $this->hash_pass($password, $salt);
		if($hash == $test)
			return TRUE;
		return FALSE;
	}
}

/* End of file Password.php */
/* Location: ./application/libraries/Password.php */
