<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function tidy(){
	$CI =& get_instance();
	$buffer = $CI->output->get_output();


	// Using PHP5 Tidy package
	//$options = array(
	//	'markup' => TRUE,
	//	'clean' => FALSE,
	//	'hide-comments' => FALSE,
	//	'indent' => FALSE,
	//	'tidy-mark' => FALSE,
	//	'indent-spaces' => 4,
	//	'new-blocklevel-tags' => 'article,header,footer,section,nav',
	//	'new-inline-tags' => 'video,audio,canvas,ruby,rt,rp',
	//	'doctype' => '<!DOCTYPE html>',
	//	'sort-attributes' => 'alpha',
	//	'vertical-space' => FALSE,
	//	'output-xhtml' => FALSE,
	//	'wrap' => 180,
	//	'wrap-attributes' => FALSE,
	//	'break-before-br' => TRUE
	//);
	//$buffer = tidy_parse_string($buffer, $options, 'utf8');
	//tidy_clean_repair($buffer);

	// Remove all spacing
	$search = array(
		'/\n/',			// replace end of line by a space
		'/\>[^\S ]+/s',		// strip whitespaces after tags, except space
		'/[^\S ]+\</s',		// strip whitespaces before tags, except space
		'/(\s)+/s'		// shorten multiple whitespace sequences
	);

	$replace = array(
		' ',
		'>',
		'<',
		'\\1'
	);

	$buffer = preg_replace($search, $replace, $buffer);

	$CI->output->set_output($buffer);
	$CI->output->_display();
}

/* End of file tidy.php */
/* Location: ./application/hooks/tidy.php */
