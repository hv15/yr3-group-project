<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
	}
	
	function _admin_check($title = 'admin')
	{
		$data = $this->userdetails->user_session($title);
		if($data['admin'] == FALSE || $data['logged'] == FALSE)
			return FALSE;
		return $data;
	}

	function index()
	{
		if(($data = $this->_admin_check()) === FALSE)
			redirect('home');
			
		$this->load->view('include/head', $data);
		$this->load->view('admin/admin_v');
		$this->load->view('include/foot', $data);
	}
	
	function staff($operation = NULL)
	{
		if(($data = $this->_admin_check()) === FALSE)
			redirect('home');

		switch($operation)
		{
			case NULL:
				$this->load->view('include/head', $data);
				$this->load->view('admin/admin_staff_v');
				$this->load->view('include/foot', $data);
				break;
			case 'edit':
				$post = $this->input->post();
				$this->load->model('staff_model','staff',TRUE);
				if($post['name'] == 'username')
				{
					$this->load->model('login_model','login',TRUE);
					if(!($this->login->change_details($post['pk'],$post['name'],$post['value'])))
						$this->output
						->set_header("HTTP/1.1 304 Not Modified")
						->set_output("User Change Failed");
				}
				if($this->staff->change_details($post['pk'],$post['name'],$post['value']))
					$this->output
					->set_header("HTTP/1.1 200 OK")
					->set_output("Worked");
				else
					$this->output
					->set_header("HTTP/1.1 304 Not Modified")
					->set_output("Failed");
				break;
			case 'add':
				$post = $this->input->post();
				$this->load->model('login_model','login',TRUE);
				$this->load->model('staff_model','staff',TRUE);
				if($this->login->add_details($post['username'],$post['password'],'staff'))
					if($this->staff->add_details($post['username'],$post['firstname'],$post['lastname'],$post['role'],$post['address'],$post['telephone'],$post['email']))
						$this->output
						->set_header("HTTP/1.1 200 OK")
						->set_output("Transaction Completed");
					else
						$this->output
						->set_header("HTTP/1.1 304 Not Modified")
						->set_output("Transaction Failed on Staff Addition");
				else
					$this->output
					->set_header("HTTP/1.1 304 Not Modified")
					->set_output("Transaction Failed on User Addition");
				break;
			case 'del':
				$post = $this->input->post();
				$this->load->model('staff_model','staff',TRUE);
				$this->load->model('login_model','login',TRUE);
				if(!($this->login->remove_details($post['pk'])))
					$this->output
					->set_header("HTTP/1.1 304 Not Modified")
					->set_output("User Change Failed");
				if($this->staff->remove_details($post['pk']))
					$this->output
					->set_header("HTTP/1.1 200 OK")
					->set_output("Worked");
				else
					$this->output
					->set_header("HTTP/1.1 304 Not Modified")
					->set_output("Failed");
				break;
			default:
				show_404("admin/staff/$operation");
				break;
		}
	}

	function events($input = NULL)
	{
		if(($data = $this->_admin_check()) === FALSE)
			redirect('home');

		switch($input)
		{
			case NULL:
				$data['sports'] = $this->userdetails->sports_array();
				$this->load->view('include/head', $data);
				$this->load->view('admin/admin_event_v');
				$this->load->view('include/foot', $data);
				break;
			case 'edit':
				$post = $this->input->post();
				$this->load->model('match_model','events',TRUE);
				if($this->events->change_details($post['pk'],$post['name'],$post['value']))
					$this->output
					->set_header("HTTP/1.1 200 OK")
					->set_output("Worked");
				else
					$this->output
					->set_header("HTTP/1.1 304 Not Modified")
					->set_output("Failed");
				break;
			case 'add':
				$post = $this->input->post();
				$this->load->model('match_model','events',TRUE);
				if($this->events->add_details($post['title'],$post['start'],$post['end'],$post['sport'],$post['umpire'],$post['location']))
					$this->output
					->set_header("HTTP/1.1 200 OK")
					->set_output("Worked");
				else
					$this->output
					->set_header("HTTP/1.1 304 Not Modified")
					->set_output("Failed");
				break;
			case 'del':
				$post = $this->input->post();
				$this->load->model('match_model','events',TRUE);
				if($this->events->remove_details($post['pk']))
					$this->output
					->set_header("HTTP/1.1 200 OK")
					->set_output("Worked");
				else
					$this->output
					->set_header("HTTP/1.1 304 Not Modified")
					->set_output("Failed");
				break;
			default:
				show_404("admin/users/$input");
				break;
		}
	}

	function users($input = NULL)
	{
		if(($data = $this->_admin_check()) === FALSE)
			redirect('home');

		switch($input)
		{
			case NULL:
				$this->load->view('include/head', $data);
				$this->load->view('admin/admin_users_v');
				$this->load->view('include/foot', $data);
				break;
			case 'pass':
				$post = $this->input->post();
				$this->load->model('login_model','login',TRUE);
				if($this->login->change_password($post['username'], $post['password']))
					$this->output
					->set_header("HTTP/1.1 200 OK")
					->set_output("Worked");
				else
					$this->output
					->set_header("HTTP/1.1 304 Not Modified")
					->set_output("Failed");
				break;
			case 'hidden':
				$post = $this->input->post();
				$this->load->model('login_model','login',TRUE);
				$value = $post['value'] == '1' ? 1 : NULL;
				if($this->login->change_details($post['pk'],$post['name'],$value))
					$this->output
					->set_header("HTTP/1.1 200 OK")
					->set_output("Worked");
				else
					$this->output
					->set_header("HTTP/1.1 304 Not Modified")
					->set_output("Failed");
				break;
			default:
				show_404("admin/users/$input");
				break;
		}
	}
	
	function competitors($input = NULL){
		if(($data = $this->_admin_check()) === FALSE)
			redirect('home');

		switch($input)
		{
			case NULL:
				$data['sports'] = $this->userdetails->sports_array();
				$data['teams'] = $this->userdetails->teams_array();
				$this->load->view('include/head', $data);
				$this->load->view('admin/admin_competitor_v', $data);
				$this->load->view('include/foot', $data);
				break;
			case 'edit':
				$post = $this->input->post();
				$this->load->model('competitor_model','competitor',TRUE);
				if($post['name'] == 'username')
				{
					$this->load->model('login_model','login',TRUE);
					if(!($this->login->change_details($post['pk'],$post['name'],$post['value'])))
						$this->output
						->set_header("HTTP/1.1 304 Not Modified")
						->set_output("User Change Failed");
				}
				if($this->competitor->change_details($post['pk'],$post['name'],$post['value']))
					$this->output
					->set_header("HTTP/1.1 200 OK")
					->set_output("Worked");
				else
					$this->output
					->set_header("HTTP/1.1 304 Not Modified")
					->set_output("Failed");
				break;
			case 'add':
				$post = $this->input->post();
				$this->load->model('login_model','login',TRUE);
				$this->load->model('competitor_model','competitor',TRUE);
				if($this->login->add_details($post['username'],$post['password'],'competitor'))
					if($this->competitor->add_details($post['username'],$post['firstname'],$post['lastname'],$post['dob'],$post['address'],$post['telephone'],$post['email'],$post['sport'],$post['team']))
						$this->output
						->set_header("HTTP/1.1 200 OK")
						->set_output("Transaction Completed");
					else
						$this->output
						->set_header("HTTP/1.1 304 Not Modified")
						->set_output("Transaction Failed on Competitor Addition");
				else
					$this->output
					->set_header("HTTP/1.1 304 Not Modified")
					->set_output("Transaction Failed on Competitor Addition");
				break;
			case 'del':
				$post = $this->input->post();
				$this->load->model('competitor_model','competitor',TRUE);
				$this->load->model('login_model','login',TRUE);
				if(!($this->login->remove_details($post['pk'])))
					$this->output
					->set_header("HTTP/1.1 304 Not Modified")
					->set_output("User Change Failed");
				if($this->competitor->remove_details($post['pk']))
					$this->output
					->set_header("HTTP/1.1 200 OK")
					->set_output("Worked");
				else
					$this->output
					->set_header("HTTP/1.1 304 Not Modified")
					->set_output("Failed");
				break;
			default:
				show_404("admin/competitors/$input");
				break;
		}
	}
	
	function teams($input = NULL){
		if(($data = $this->_admin_check()) === FALSE)
			redirect('home');

		switch($input)
		{
			case NULL:
				$this->load->view('include/head', $data);
				$this->load->view('admin/admin_team_v', $data);
				$this->load->view('include/foot', $data);
				break;
			case 'edit':
				$post = $this->input->post();
				$this->load->model('team_model','team',TRUE);
				if($post['name'] == 'username')
				{
					$this->load->model('login_model','login',TRUE);
					if(!($this->login->change_details($post['pk'],$post['name'],$post['value'])))
						$this->output
						->set_header("HTTP/1.1 304 Not Modified")
						->set_output("User Change Failed");
				}
				if($this->team->change_details($post['pk'],$post['name'],$post['value']))
					$this->output
					->set_header("HTTP/1.1 200 OK")
					->set_output("Worked");
				else
					$this->output
					->set_header("HTTP/1.1 304 Not Modified")
					->set_output("Failed");
				break;
			case 'add':
				$post = $this->input->post();
				$this->load->model('login_model','login',TRUE);
				$this->load->model('team_model','team',TRUE);
				if($this->login->add_details($post['username'],$post['password'],'team',1))
					if($this->team->add_details($post['username'],$post['team'],$post['coachfname'],$post['coachlname'],$post['address'],$post['telephone'],$post['email'],$post['memnum'],$post['membership'],$post['membershipnum']))
						$this->output
						->set_header("HTTP/1.1 200 OK")
						->set_output("Transaction Completed");
					else
						$this->output
						->set_header("HTTP/1.1 304 Not Modified")
						->set_output("Transaction Failed on Competitor Addition");
				else
					$this->output
					->set_header("HTTP/1.1 304 Not Modified")
					->set_output("Transaction Failed on Competitor Addition");
				break;
			case 'del':
				$post = $this->input->post();
				$this->load->model('team_model','team',TRUE);
				$this->load->model('login_model','login',TRUE);
				if(!($this->login->remove_details($post['pk'])))
					$this->output
					->set_header("HTTP/1.1 304 Not Modified")
					->set_output("User Change Failed");
				if($this->team->remove_details($post['pk']))
					$this->output
					->set_header("HTTP/1.1 200 OK")
					->set_output("Worked");
				else
					$this->output
					->set_header("HTTP/1.1 304 Not Modified")
					->set_output("Failed");
				break;
			default:
				show_404("admin/teams/$input");
				break;
		}
	}
	
	function sports($input = NULL){
		if(($data = $this->_admin_check()) === FALSE)
			redirect('home');

		switch($input)
		{
			case NULL:
				$this->load->view('include/head', $data);
				$this->load->view('admin/admin_sport_v');
				$this->load->view('include/foot', $data);
				break;
			case 'edit':
				$post = $this->input->post();
				$this->load->model('sport_model','sport',TRUE);
				if($this->sport->change_details($post['pk'],$post['name'],$post['value']))
					$this->output
					->set_header("HTTP/1.1 200 OK")
					->set_output("Worked");
				else
					$this->output
					->set_header("HTTP/1.1 304 Not Modified")
					->set_output("Failed");
				break;
			case 'add':
				$post = $this->input->post();
				$this->load->model('sport_model','sport',TRUE);
				if($this->sport->add_details($post['sport']))
					$this->output
					->set_header("HTTP/1.1 200 OK")
					->set_output("Transaction Completed");
				else
					$this->output
					->set_header("HTTP/1.1 304 Not Modified")
					->set_output("Transaction Failed on Position Addition");
				break;
			case 'del':
				$post = $this->input->post();
				$this->load->model('sport_model','sport',TRUE);
				if($this->sport->remove_details($post['pk']))
					$this->output
					->set_header("HTTP/1.1 200 OK")
					->set_output("Worked");
				else
					$this->output
					->set_header("HTTP/1.1 304 Not Modified")
					->set_output("Failed");
				break;
			default:
				show_404("admin/sports/$input");
				break;
		}
	}
	
	function table($table = null)
	{
		if(!empty($table))
		{
			switch($table)
			{
				/* Small hack here needed because of admin panel tables having a 'null' column */
				case 'staff':
					$columns = array('','username','firstname','lastname','role','address','telephone','email');
					$this->load->model('staff_model', 'data_source', TRUE);
					break;
				case 'users':
					$columns = array('','username', 'role', 'hidden');
					$this->load->model('login_model','data_source',TRUE);
					break;
				case 'competitors':
					$columns = array('','username','firstname','lastname','dob','address','telephone','email','sport','team');
					$this->load->model('competitor_model','data_source',TRUE);
					break;
				case 'umpires':
					$columns = array('','username','firstname','lastname','address','telephone','email');
					$this->load->model('umpire_model','data_source',TRUE);
					break;
				case 'matches':
					$columns = array('','title','start','end','sport','umpire','location');
					$this->load->model('match_model','data_source',TRUE);
					break;
				case 'sports':
					$columns = array('sport');
					$this->load->model('sport_model','data_source',TRUE);
					break;
				case 'teams':
					$columns = array('','username','team','coachfname','coachlname','address','telephone','email','membernum','membership','membershipnum');
					$this->load->model('team_model','data_source',TRUE);
					break;
				default:
					// error
					show_error("$table does not match any case");
					break;
			}
			
			$get = $this->input->get();
			
			// START OF CACHED QUERY !
			$this->db->start_cache();
			
			// Ordering
			if(isset($get['iSortCol_0']))
			{
				for($i = 0; $i < intval($get['iSortingCols']); $i++)
				{
					if($get['bSortable_'.intval($get['iSortCol_'.$i])] == 'true')
					{
						$this->data_source->order($columns[intval($get['iSortCol_'.$i])], ($get['sSortDir_'.$i] === 'asc' ? 'asc' : 'desc'));
					}	
				}
			}
			
			// Filtering Generic
			if(isset($get['sSearch']) && !empty($get['sSearch']))
			{
				//echo 'Search for "'.$get['sSearch'].'"';
				for($i = 0; $i < count($columns); $i++)
				{
					$this->data_source->or_like($columns[$i], $get['sSearch']);
					//echo 'Loop 1, round '.$i;
				}
			}
			
			// Filtering Column Specific 
			for($i = 0; $i < count($columns); $i++)
			{
				if(isset($get['bSearchable_'.$i]) && $get['bSearchable_'.$i] == 'true' && !empty($get['sSearch_'.$i]))
				{
					//echo "This -> {$columns[$i]}, {$get['sSearch_'.$i]}";
					$this->data_source->like($columns[$i], $get['sSearch_'.$i]);
					//echo 'Loop 2, round '.$i;
				}
			}
			
			$this->db->stop_cache();
			// END OF CACHED QUERY !
			
			$numfilter = $this->data_source->get_result_count();
			$table = $this->data_source->get_table('*', $get['iDisplayLength'], $get['iDisplayStart']);
			
			// FLUSH THE QUERY CACHE !
			$this->db->flush_cache();

			$output = array(
				'aaData' => $this->_get_rows($table),
				'sEcho' => intval($get['sEcho']),
				'iTotalRecords' => $this->data_source->get_all_count(),
				'iTotalDisplayRecords' => $numfilter
			);
			
			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($output));
		}
	}

	function _get_columns($table)
	{
		$columns = array();
		if(!empty($table))
		{
			$keys = array_keys($table[0]);
			foreach($keys as $key)
			{
				array_push($columns, array('mData' => $key));
			}
		}
		return $columns;
	}

	function _get_rows($table)
	{
		
		$rows = array();
		if(!empty($table))
		{
			$objects = array_values($table);
			foreach($objects as $object)
			{
				$values = array_map('nl2br', $object);
				array_push($rows, $values);
			}
		}
		return $rows;
	}
}

/* End of file admin.php */
/* Location: ./application/controllers/admin.php */