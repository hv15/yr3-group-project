<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Stats extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
	}
	
	function sport($select = TRUE)
	{
		$this->load->model('sport_model','sport',TRUE);
		$sports = $this->sport->get_table();
		$data = array();
		
		switch($select)
		{
			case FALSE:
				// not sure
				break;
			default:
				foreach($sports as $sport)
				{
					array_push($data, array('value' => $sport['sport'], 'text' => $sport['sport']));
				}
				$this->output
				->set_content_type('application/json')
				->set_output(json_encode($data));
				break;
		}
	}
	
	function team($select = TRUE)
	{
		$this->load->model('team_model','team',TRUE);
		$teams = $this->team->get_table();
		$data = array();
		
		switch($select)
		{
			case FALSE:
				// not sure
				break;
			default:
				foreach($teams as $team)
				{
					array_push($data, array('value' => $team['team'], 'text' => $team['team']));
				}
				$this->output
				->set_content_type('application/json')
				->set_output(json_encode($data));
				break;
		}
	}
}

/* End of file stats.php */
/* Location: ./application/controllers/stats.php */