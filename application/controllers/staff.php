<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Staff extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->model('Staff_model','staff',TRUE);
	}

	public function index()
	{
		// Empty
	}
}

/* End of file staff.php */
/* Location: ./application/controllers/staff.php */
