<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Event extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
	}

	function index()
	{
		$data = $this->userdetails->user_session('events');
		$this->load->view('include/head', $data);
		$this->load->view('event/event_v');
		$this->load->view('include/foot', $data);
	}
	
	function get()
	{
		$post = $this->input->post();
		$this->load->model('match_model','match',TRUE);
		$matches = $this->match->get_by_interval($post['start'], $post['end']);
		//$matches = $this->match->get_table();
		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($this->_fullcalendar($matches)));
	}
	
	function _fullcalendar($arrays)
	{
		for($key = 0; $key < count($arrays); $key++)
		{
			$arrays[$key]['allDay'] = FALSE;
		}
		return $arrays;
	}
}

/* End of file event.php */
/* Location: ./application/controllers/event.php */
