<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
	}

	function index()
	{
		$data = $this->userdetails->user_session('home');
		$data['events'] = $this->events();
		$this->load->view('include/head', $data);
		$this->load->view('home/home_v', $data);
		$this->load->view('include/foot', $data);
	}
	
	function terms()
	{
		$data = $this->userdetails->user_session('terms');
		$this->load->view('include/head', $data);
		$this->load->view('home/term_v', $data);
		$this->load->view('include/foot', $data);
	}
	
	function events()
	{
		$this->load->model('match_model','match',TRUE);
		$matches = $this->match->get_by_interval(time() - 43200, time() + 432000);
		return $matches;
	}
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */
