<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
	}

	function index()
	{
		$data = $this->userdetails->user_session('profile');
		
		$data['teams'] = $this->userdetails->details_array('team');
		$data['competitors'] = $this->userdetails->details_array('competitor');
		
		$this->load->view('include/head', $data);
		$this->load->view('profile/profile_v', $data);
		$this->load->view('include/foot', $data);
	}
	
	function user($user = NULL, $status = 'view')
	{
		$edit_button = ''; // default button does not exist
		$data = $this->userdetails->user_session('profile');
		if(empty($user) && empty($data['user']))
			$this->index();
		else
			$user = empty($user) ? $data['user'] : $user;
		
		$this->load->model('login_model','login',TRUE);
		$account = $this->login->get_details($user);
		
		if(($account == FALSE || empty($account[0]['hidden'])) && ($data['admin'] == FALSE && (empty($account[0]['username']) || $account[0]['username'] != $data['user'])))
			$this->index();
		else {
			$info = $this->userdetails->get_details($account[0]['username'],$account[0]['role']);
			if($data['admin'] == TRUE || ($data['user'] == $account[0]['username']))
				$edit_button = "<small><a href=\"".current_url()."/edit\" class=\"btn btn-info pull-right\">Edit</a></small>";
			else
			{
				if($status == 'edit')
					redirect("profile/$user");
			}
			
			$this->load->view('include/head', $data);
			$post_url = site_url("login/edit/{$account[0]['role']}");
			switch($account[0]['role'])
			{
				case 'team':
					if($status == 'view')
					{
						$members = $this->userdetails->team_member_array($account[0]['username']);
						$members_list = '';
						if(!empty($members))
						{
							foreach($members as $member)
							{
								$members_list .= "<li><a href=\"".site_url("profile/{$member['username']}")."\">{$member['firstname']} {$member['lastname']}</a></li>\n";
							}
						}
						$this->output->append_output("
		<div class=\"container\">
			<div class=\"row\">
				<div class=\"span12\">
					<div class=\"page-header\">
						<h3>{$info[0]['team']} <small>".ucfirst($account[0]['role'])."</small>$edit_button</h3>
					</div>
				</div>
			</div>
			<div class=\"row\">
				<div class=\"span2\">
					<img src=\"".base_url('img/avatar.png')."\" class=\"img-polaroid\">
				</div>
				<div class=\"span10\">
					<div class=\"well\">
						<dl>
							<dt>Coach</dt>
							<dd>{$info[0]['coachfname']} {$info[0]['coachlname']}</dd>
							<dt>Address</dt>
							<dd>{$info[0]['address']}</dd>
							<dt>About</dt>
							<dd>{$info[0]['about']}</dd>
							<dt>Members</dt>
							<dd>
								<ul class=\"unstyled\">
									$members_list
								</ul>
							</dd>
						</dl>
					</div>
				</div>
			</div>
		</div>
");
					} else {
						$this->output->append_output("
		<div class=\"container\">
			<div class=\"row\">
				<div class=\"span12\">
					<div class=\"page-header\">
						<a href=\"".site_url("profile/{$account[0]['username']}")."\" class=\"btn btn-success btn-large\"><i class=\"icon-arrow-left\"></i> Go Back</a>
					</div>
				</div>
			</div>
			<div class=\"row\">
				<div class=\"span2\">
					<img src=\"".base_url('img/avatar.png')."\" class=\"img-polaroid\">
				</div>
				<div class=\"span10\">
					<div class=\"well\">
						<table class=\"table table-condensed table-borderless\">
							<tbody>
								<tr>
									<td><strong>Team Name: </strong></td>
									<td><a href=\"#\" class=\"editable\" id=\"team\" data-type=\"text\" data-pk=\"{$info[0]['username']}\" data-url=\"$post_url\" data-original-title=\"Enter first name\">{$info[0]['team']}</a></td>
								</tr>
								<tr>
									<td><strong>Email: </strong></td>
									<td><a href=\"#\" class=\"editable\" id=\"email\" data-type=\"email\" data-pk=\"{$info[0]['username']}\" data-url=\"$post_url\" data-original-title=\"Enter first name\">{$info[0]['email']}</a></td>
								</tr>
								<tr>
									<td><strong>Coach Name: </strong></td>
									<td>
										<a href=\"#\" class=\"editable\" id=\"coachfname\" data-type=\"text\" data-pk=\"{$info[0]['username']}\" data-url=\"$post_url\" data-original-title=\"Enter first name\">{$info[0]['coachfname']}</a>
										<a href=\"#\" class=\"editable\" id=\"coachlname\" data-type=\"text\" data-pk=\"{$info[0]['username']}\" data-url=\"$post_url\" data-original-title=\"Enter first name\">{$info[0]['coachlname']}</a>
									</td>
								</tr>
								<tr>
									<td><strong>Address: </strong></td>
									<td><a href=\"#\" class=\"editable\" id=\"address\" data-type=\"textarea\" data-pk=\"{$info[0]['username']}\" data-url=\"$post_url\" data-original-title=\"Enter first name\">{$info[0]['address']}</a></td>
								</tr>
								<tr>
									<td><strong>About: </strong></td>
									<td><a href=\"#\" class=\"editable\" id=\"about\" data-type=\"textarea\" data-pk=\"{$info[0]['username']}\" data-url=\"$post_url\" data-original-title=\"Enter first name\">{$info[0]['about']}</a></td>
								</tr>
								<tr>
									<td><a data-toggle=\"modal\" href=\"#change_pw_modal\" data-pk=\"{$info[0]['username']}\" class=\"btn btn-primary pwd\"><i class=\"icon-lock\"></i> Change Password</a></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<!-- Modal CHANGE PASSWORD -->
		<div id=\"change_pw_modal\" class=\"modal hide fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"change_pw_modal\" aria-hidden=\"true\">
			<form id=\"change_pw_form\" class=\"modal-form form-horizontal\" action=\"".site_url('login/edit/pass')."\">
				<div class=\"modal-header\">
					<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>
					<h3 id=\"add_row_modal_label\">Change User Password</h3>
				</div>
				<div class=\"modal-body\">
					<div class=\"alert alert-error fade\">
						<strong>Error!</strong> Transaction was not completed!
					</div>
					<div class=\"control-group\">
						<label class=\"control-label\" for=\"username\">Username</label>
						<div class=\"controls\">
							<input type=\"text\" id=\"username\" name=\"username\" placeholder=\"Username\" readonly/>
							<p class=\"help-block\"></p>
						</div>
					</div>
					<div class=\"control-group\">
						<label class=\"control-label\" for=\"password\">Password</label>
						<div class=\"controls\">
							<input type=\"password\" id=\"password\" name=\"password\" maxlength=\"25\" placeholder=\"Password\" required/>
							<p class=\"help-block\"></p>
						</div>
					</div>
					<div class=\"control-group\">
						<label class=\"control-label\" for=\"passwordr\">Lastname</label>
						<div class=\"controls\">
							<input type=\"password\" data-validation-match-match=\"password\" id=\"passwordr\" name=\"passwordr\" maxlength=\"25\" placeholder=\"Repeat\" required/>
							<p class=\"help-block\"></p>
						</div>
					</div>
				</div>
				<div class=\"modal-footer\">
					<button id=\"reset\" type=\"reset\" class=\"btn\">Reset</button>
					<button id=\"submit\" type=\"submit\" class=\"btn btn-primary\">Save</button>
				</div>
			</form>
		</div>
");					
					}
					break;
				case 'competitor':
					if($status == 'view')
					{
						$team_name = $this->userdetails->team_name($info[0]['team']);
						$this->output->append_output("
		<div class=\"container\">
			<div class=\"row\">
				<div class=\"span12\">
					<div class=\"page-header\">
						<h3>{$info[0]['firstname']} {$info[0]['lastname']} <small>".ucfirst($account[0]['role'])."</small>$edit_button</h3>
					</div>
				</div>
			</div>
			<div class=\"row\">
				<div class=\"span2\">
					<img src=\"".base_url('img/avatar.png')."\" class=\"img-polaroid\">
				</div>
				<div class=\"span10\">
					<div class=\"well\">
						<dl>
							<dt>Sport</dt>
							<dd>".ucfirst($info[0]['sport'])."</dd>
							<dt>Team</dt>
							<dd><a href=\"".site_url("profile/{$info[0]['team']}")."\">$team_name</a></dd>
							<dt>About</dt>
							<dd>{$info[0]['about']}</dd>
						</dl>
					</div>
				</div>
			</div>
		</div>
");
					} else {
						$this->output->append_output("
		<div class=\"container\">
			<div class=\"row\">
				<div class=\"span12\">
					<div class=\"page-header\">
						<a href=\"".site_url("profile/{$account[0]['username']}")."\" class=\"btn btn-success btn-large\"><i class=\"icon-arrow-left\"></i> Go Back</a>
					</div>
				</div>
			</div>
			<div class=\"row\">
				<div class=\"span2\">
					<img src=\"".base_url('img/avatar.png')."\" class=\"img-polaroid\">
				</div>
				<div class=\"span10\">
					<div class=\"well\">
						<table class=\"table table-condensed table-borderless\">
							<tbody>
								<tr>
									<td><strong>Full Name: </strong></td>
									<td>
										<a href=\"#\" class=\"editable\" id=\"firstname\" data-type=\"text\" data-pk=\"{$info[0]['username']}\" data-url=\"$post_url\" data-original-title=\"Enter first name\">{$info[0]['firstname']}</a>
										<a href=\"#\" class=\"editable\" id=\"lastname\" data-type=\"text\" data-pk=\"{$info[0]['username']}\" data-url=\"$post_url\" data-original-title=\"Enter last name\">{$info[0]['lastname']}</a>
									</td>
								</tr>
								<tr>
									<td><strong>Email: </strong></td>
									<td><a href=\"#\" class=\"editable\" id=\"email\" data-type=\"email\" data-pk=\"{$info[0]['username']}\" data-url=\"$post_url\" data-original-title=\"Enter first name\">{$info[0]['email']}</a></td>
								</tr>
								<tr>
									<td><strong>Date of Birth: </strong></td>
									<td><a href=\"#\" class=\"editable\" id=\"dob\" data-type=\"date\" data-pk=\"{$info[0]['username']}\" data-url=\"$post_url\" data-original-title=\"Enter first name\">{$info[0]['dob']}</a></td>
								</tr>
								<tr>
									<td><strong>Address: </strong></td>
									<td><a href=\"#\" class=\"editable\" id=\"address\" data-type=\"textarea\" data-pk=\"{$info[0]['username']}\" data-url=\"$post_url\" data-original-title=\"Enter first name\">{$info[0]['address']}</a></td>
								</tr>
								<tr>
									<td><strong>About: </strong></td>
									<td><a href=\"#\" class=\"editable\" id=\"about\" data-type=\"textarea\" data-pk=\"{$info[0]['username']}\" data-url=\"$post_url\" data-original-title=\"Enter first name\">{$info[0]['about']}</a></td>
								</tr>
								<tr>
									<td><strong>Hidden: </strong></td>
									<td><a href=\"#\" class=\"editable\" id=\"hidden\" data-type=\"select\" data-source=\"[{value: 'NULL', text: 'Yes'}, {value: 1, text: 'No'}]\" data-pk=\"{$info[0]['username']}\" data-url=\"".site_url('login/hide')."\" data-original-title=\"Enter first name\"></a></td>
								</tr>
								<tr>
									<td><a data-toggle=\"modal\" href=\"#change_pw_modal\" data-pk=\"{$info[0]['username']}\" class=\"btn btn-primary pwd\"><i class=\"icon-lock\"></i> Change Password</a></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<!-- Modal CHANGE PASSWORD -->
		<div id=\"change_pw_modal\" class=\"modal hide fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"change_pw_modal\" aria-hidden=\"true\">
			<form id=\"change_pw_form\" class=\"modal-form form-horizontal\" action=\"".site_url('login/edit/pass')."\">
				<div class=\"modal-header\">
					<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>
					<h3 id=\"add_row_modal_label\">Change User Password</h3>
				</div>
				<div class=\"modal-body\">
					<div class=\"alert alert-error fade\">
						<strong>Error!</strong> Transaction was not completed!
					</div>
					<div class=\"control-group\">
						<label class=\"control-label\" for=\"username\">Username</label>
						<div class=\"controls\">
							<input type=\"text\" id=\"username\" name=\"username\" placeholder=\"Username\" readonly/>
							<p class=\"help-block\"></p>
						</div>
					</div>
					<div class=\"control-group\">
						<label class=\"control-label\" for=\"password\">Password</label>
						<div class=\"controls\">
							<input type=\"password\" id=\"password\" name=\"password\" maxlength=\"25\" placeholder=\"Password\" required/>
							<p class=\"help-block\"></p>
						</div>
					</div>
					<div class=\"control-group\">
						<label class=\"control-label\" for=\"passwordr\">Lastname</label>
						<div class=\"controls\">
							<input type=\"password\" data-validation-match-match=\"password\" id=\"passwordr\" name=\"passwordr\" maxlength=\"25\" placeholder=\"Repeat\" required/>
							<p class=\"help-block\"></p>
						</div>
					</div>
				</div>
				<div class=\"modal-footer\">
					<button id=\"reset\" type=\"reset\" class=\"btn\">Reset</button>
					<button id=\"submit\" type=\"submit\" class=\"btn btn-primary\">Save</button>
				</div>
			</form>
		</div>
");					
					}
					break;
				default:
					show_404("profile/$user/$status");
			}

			/* JAVASCRIPT FOR THE PAGE */
			$this->output->append_output("
		<script type=\"text/javascript\" charset=\"utf-8\">
			head.js(
					{'editable': '".base_url('js/bootstrap-editable.min.js')."'},
					{'validate': '".base_url('js/jqBootstrapValidation.js')."'},
					'".base_url('css/bootstrap-editable.css')."'
			);
			head.ready(function(){
				$.fn.editable.defaults.mode = 'inline';
				$('.editable').editable();
				$('table').on('click', '.pwd', function(){
					$('#change_pw_form #username').attr('value', $(this).attr('data-pk'));
				});
				$(function(){
					$('input,select').not('[type=submit]').jqBootstrapValidation();
				});
				
				$('#change_pw_form').submit(function(){
					$.post(this.action, $(this).serialize(), function(response, data, jqXHR){
						if(jqXHR.status == 200){
							$('#change_pw_modal').modal('hide');
							$('#change_pw_modal').on('hidden',function(){
								// reset not working...
								//$('#add_row_form').each(function(){ this.reset(); });
							});
						} else {
							// this can be done better!
							$('#change_pw_modal .alert-error').addClass('in');
						}
					});
					return false;
				});
			});
		</script>
");
			$this->load->view('include/foot', $data);
		}
	}
}

/* End of file profile.php */
/* Location: ./application/controllers/profile.php */
