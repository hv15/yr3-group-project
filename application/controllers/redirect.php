<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Redirect extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->library('session');
	}

	function index(){
		if($this->session->userdata('admin'))
			redirect('admin');
		else
			redirect('home');
	}
}
/* End of file redirect.php */
/* Location: ./application/controllers/redirect.php */
