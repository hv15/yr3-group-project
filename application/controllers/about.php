<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class About extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
	}

	function index()
	{
		$data = array(
			'title' => 'about'
		);
		if(($user = $this->session->userdata('user')) !== FALSE)
		{
			$data['user'] = $user;
			$data['admin'] = $this->session->userdata('admin');
			$data['logged'] = $this->session->userdata('logged');
		}
		$this->load->view('include/head', $data);
		$this->load->view('about/about_v');
		$this->load->view('include/foot', $data);
	}
}

/* End of file about.php */
/* Location: ./application/controllers/about.php */
