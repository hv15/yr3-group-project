<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->model('login_model','login',TRUE);
	}

	function index()
	{
		if($this->input->server('REQUEST_METHOD') != 'POST')
			redirect('redirect');
		
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$details = $this->login->get_details($username, TRUE);
		
		if(empty($details)) // think about, we need error correction...
			redirect('/');
			
		if($this->password->is_valid_pass($details[0]['password'],$password)){
			$this->load->library('session');
			$mdetails = $this->userdetails->get_details($username, $details[0]['role']);
			$this->session->set_userdata(array('user' => $username, 'admin' => $this->_is_admin($mdetails[0]), 'logged' => TRUE));
			redirect('redirect');
		}
		else
		{
			redirect('/');
		}
	}
	
	function register($operation = NULL)
	{
		switch($operation){
			case NULL:
				$data['sports'] = $this->userdetails->sports_array();
				$data['teams'] = $this->userdetails->teams_array();
				$this->load->view('include/head');
				$this->load->view('register/register_v', $data);
				$this->load->view('include/foot');
				break;
			case 'comp':
				//print_r($this->input->post());
				$post = $this->input->post();
				$this->load->model('login_model','login',TRUE);
				$this->load->model('competitor_model','competitor',TRUE);
				if($this->login->add_details($post['username'],$post['password'],'competitor'))
					if($this->competitor->add_details($post['username'],$post['fname'],$post['sname'],$post['dob'],$post['address'],$post['telephone'],$post['email'],$post['sport'],$post['team']))
						$this->output
						->set_header("HTTP/1.1 200 OK")
						->set_output("Transaction Completed");
					else
						$this->output
						->set_header("HTTP/1.1 304 Not Modified")
						->set_output("Transaction Failed on Competitor Addition");
				else
					$this->output
					->set_header("HTTP/1.1 304 Not Modified")
					->set_output("Transaction Failed on Competitor Addition");
				break;
			case 'team':
				$post = $this->input->post();
				$this->load->model('login_model','login',TRUE);
				$this->load->model('team_model','team',TRUE);
				if($this->login->add_details($post['username'],$post['password'],'team',1))
					if($this->team->add_details($post['username'],$post['tname'],$post['fname'],$post['sname'],$post['address'],$post['telephone'],$post['email'],$post['memnum'],$post['membership'],$post['membershipnum']))
						$this->output
						->set_header("HTTP/1.1 200 OK")
						->set_output("Transaction Completed");
					else
						$this->output
						->set_header("HTTP/1.1 304 Not Modified")
						->set_output("Transaction Failed on Competitor Addition");
				else
					$this->output
					->set_header("HTTP/1.1 304 Not Modified")
					->set_output("Transaction Failed on Competitor Addition");
				break;
			default:
				show_404("login/register/$operation");
				break;
		}
	}
	
	function edit($role = NULL)
	{
		$data = $this->userdetails->user_session('profile');
		$post = $this->input->post();
		
		if(($this->input->server('REQUEST_METHOD') != 'POST') || ($data['admin'] == FALSE && ($post['pk'] != $data['user'])))
		{
			$this->output
			->set_header("HTTP/1.1 404 Not Found")
			->set_output("Page Unknown");
		}
		else
		{
			switch($role)
			{
				case 'competitor':
					$this->load->model('competitor_model','table',TRUE);
					break;
				case 'team':
					$this->load->model('team_model','table',TRUE);
					break;
				case 'pass':
					$this->load->model('login_model','pass',TRUE);
					if($this->pass->change_password($post['username'],$post['password']))
						$this->output
						->set_header("HTTP/1.1 200 OK")
						->set_output("Worked");
					else
						$this->output
						->set_header("HTTP/1.1 304 Not Modified")
						->set_output("Failed");
					return;
					break;
				default:
					$this->output
					->set_header("HTTP/1.1 304 Not Modified")
					->set_output("No Role Given");
					break;
			}
			if($this->table->change_details($post['pk'],$post['name'],$post['value']))
				$this->output
				->set_header("HTTP/1.1 200 OK")
				->set_output("Worked");
			else
				$this->output
				->set_header("HTTP/1.1 304 Not Modified")
				->set_output("Failed");
		}
	}
	
	function hide()
	{
		$data = $this->userdetails->user_session('profile');
		$post = $this->input->post();
	
		if(($this->input->server('REQUEST_METHOD') != 'POST') || ($data['admin'] == FALSE && ($post['pk'] != $data['user'])))
		{
			$this->output
			->set_header("HTTP/1.1 404 Not Found")
			->set_output("Page Unknown");
		}
		else
		{
			if($this->login->change_details($post['pk'],$post['name'],$post['value']))
				$this->output
				->set_header("HTTP/1.1 200 OK")
				->set_output("Worked");
			else
				$this->output
				->set_header("HTTP/1.1 304 Not Modified")
				->set_output("Failed");
		}
	}

	/* UNNESSISARY--> */
	function table()
	{
		$table = $this->login->get_table();
		$data = array(
			'title' => 'Table',
			'content' => 'login_table',
			'data' => $table
		);
		$this->load->view('template',$data);
	}

	function find()
	{
		$pattern = $this->uri->segment(3,0);
		$table = $this->login->find_details($pattern, TRUE);
		$data = array(
			'title' => 'Search',
			'content' => 'login_table',
			'data' => $table
		);
		$this->load->view('template',$data);
	}
	
	function test_user($username,$password)
	{
		switch($this->_is_pass($username,$password))
		{
			case TRUE:
				$this->output
				->set_output("$username:$password is valid!");
				break;
			default:
				$this->output
				->set_output("$username:$password is invalid!");
				break;
		}
	}
	
	function hash($username,$password)
	{
		$salt = $this->password->hash_salt($username);
		echo $this->password->hash_pass($password,$salt);
	}
	/* <--UNNESSISARY */

	function _is_admin($row)
	{
		if(isset($row['admin']) && !empty($row['admin']))
			return TRUE;
		return FALSE;
	}

	function out()
	{
		$this->load->library('session');
		$this->session->unset_userdata(array('user' => '','admin' => '', 'logged' => ''));
		redirect('redirect');
	}

	function _is_pass($username,$test_password)
	{
		$details = $this->login->get_details($username, TRUE);
		if(empty($details))
			return FALSE;
		if($this->password->is_valid_pass($details[0]['password'],$test_password))
			return TRUE;
		return FALSE;
	}
	
	function exist()
	{
		$value = $this->input->get('value', TRUE);
		$this->output
		->set_content_type('application/json')
		->set_output(json_encode(
			array(
				'value' => $value,
				'valid' => !$this->login->user_exist($value),
				'message' => 'This username is taken'
			)
		));
	}
}

/* End of file login.php */
/* Location: ./application/controllers/login.php */
