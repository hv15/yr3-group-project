		<div class="container">
			<div class="row">
				<div class="span12">
					<div class="page-header">
						<h1>See what's on here at the Centre <small></small></h1>
					</div>
				</div>
			</div>	
			<div class="row">
				<div class="span12">
					<div id="calendar" class="well"></div>
				</div>
			</div>
		<!--
		<a href="#ticketmodal" role="button" class="btn" data-toggle="modal">Purchase Tickets</a>
			
			<div id="ticketmodal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="ticketmodal" aria-hidden="true">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 id="ticketmodal">Ticket Purchase</h3>
				</div>
			<div class="modal-body" >
				<div style="margin-rigth:25px;">
					<div class="alert alert-error fade">
						<strong> Error !</strong>
						"transaction was not completed !"
					</div>
				    <div class="control-group">
						<label class ="control-label" for "firstname">Firstname</label>
						<div class="controls">
							<input type="text" id="firstname" name="firstname" placeholder="Firstname" required>
							<p class="help-block"></p>
						</div>	
					</div>
					
					<div class="control-group">
						<label class ="control-label" for "lastname">Lastname</label>
						<div class="controls">
							<input type="text" id="lastname" name="lastname" placeholder="Lastname" required>
							<p class="help-block"></p>
						</div>	
					</div>
					
					<div class="control-group">
						<label class ="control-label" for "address">Address</label>
						<div class="controls">
							<input type="text" id="address" name="address" placeholder="Address" required>
							<p class="help-block"></p>
						</div>	
					</div>
					
					<div class="control-group">
						<label class ="control-label" for "telephone">Telephone</label>
						<div class="controls">
							<input type="text" id="telephone" name="telephone" placeholder="Telephone" required>
							<p class="help-block"></p>
						</div>	
					</div>
					
					<div class="control-group">
						<label class ="control-label" for "email">Email</label>
						<div class="controls">
							<input type="text" id="email" name="email" placeholder="Email" required>
							<p class="help-block"></p>
						</div>	
					</div>
					
					<div class="control-group">
					  <div class="control-label" for="dop">Date of purchase</label>
						<div class="controls">
							<input type="date" id="dop" name="dop" data-data-format="yyyy-mm-dd" placeholder="e.g 1991-01-28" required aria-invalid="false">
							<p class="help-block"></p>
					  </div>
					  
					</div>
					<div class="control-group">
						<label class ="control-label" for "status">Number of tickets</label>
						<div class="controls">
							<input type="text" id="status" name="status" placeholder="Number of tickets" required>
							<p class="help-block"></p>
						</div>	
					</div>
					
					<div class="modal-footer">
						<div style="margin-rigth:25px;">
							<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
							<button class="btn" data-dismiss="modal" aria-hidden="true">Book</button>
						</div>
					</div>
					
					
			</div>  

			</center>
		-->
		</div>
		<script type="text/javascript" charset="utf-8">
			head.js(
				{'calendar': '<?php echo base_url('js/fullcalendar.min.js');?>'},
				'<?php echo base_url('css/fullcalendar.css');?>'
			);
			head.ready(function(){
				$('#calendar').fullCalendar({
					header: {
						left: 'prev,next today',
						center: 'title',
						right: 'month,agendaWeek'
					},
					defaultView: 'month',
					events: {
						url: '<?php echo site_url('event/get');?>',
						type: 'POST',
						error: function(){
							console.log('Couldnt get data');
						}
					},
					axisFormat: { '': 'HH:mm' },
					timeFormat: {
						agenda: 'HH:mm{ - HH:mm}', // 5:00 - 6:30

						// for all other views
						'': 'HH(:mm)'            // 7p
					},
					eventRender: function(event, element){
						element.popover({
							title: event.title,
							placement: 'right',
							html: true,
							container: 'body',
							content: '<ul class="unstyled"><li>Sport: '+event.sport+'</li><li>Location: '+event.location+'</li></ul>',
							trigger: 'hover'
						});
					}
				})
			});
		</script>
