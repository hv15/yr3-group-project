<!DOCTYPE html>
<html lang="en">
	<head>
		<title><?php if(isset($title)) echo ucfirst($title).' | ';?>Riccarton Sport Centre</title>
		<link rel="stylesheet" href="<?php echo base_url('css/bootstrap.min.css');?>">
		<link rel="stylesheet" href="<?php echo base_url('css/boot.css');?>">
		<script type="text/javascript" charset="utf-8" src="//cdnjs.cloudflare.com/ajax/libs/headjs/0.99/head.min.js"></script>
		<script type="text/javascript">
			head.js(
				{'jquery': 'http://code.jquery.com/jquery-latest.js'},
				{'bootstrap': '<?php echo base_url('js/bootstrap.min.js');?>'}
			);
		</script>
	</head>
<body>
	<div id="wrap">
		<div class="navbar navbar-static-top" id="navbar">
			<div class="navbar-inner">
				<div class="container">
					<a class="brand" href="<?php echo site_url();?>">Riccarton Sport Centre</a>
					<ul class="nav">
						<li<?php if(isset($title) && $title == 'home') echo ' class="active"';?>><a href="<?php echo site_url();?>">Home</a></li>
						<li<?php if(isset($title) && $title == 'about') echo ' class="active"';?>><a href="<?php echo site_url('about');?>">About</a></li>
						<li<?php if(isset($title) && $title == 'events') echo ' class="active"';?>><a href="<?php echo site_url('event');?>">Events</a></li>
						<li<?php if(isset($title) && $title == 'profile') echo ' class="active"';?>><a href="<?php echo site_url('profile');?>">Profile</a></li>
					</ul>	
					<ul class="nav pull-right">
						<?php if(isset($user) && isset($logged) && $logged){ ?>
						<li class="dropdown">
							<a class="dropdown-toggle" href="#" data-toggle="dropdown" data-target="#">Hello, <?php echo ucfirst(trim($user));?> <i class="caret"></i></a>
							<ul class="dropdown-menu">
								<?php if(isset($admin) && $admin == TRUE){ ?>
								<li><a href="<?php echo site_url('admin');?>">Admin Panel</a></li>
								<?php } ?>
								<li><a href="<?php echo site_url("profile/$user/edit");?>">Edit Profile</a></li>
							</ul>
						</li>
						<li class="divider-vertical"></li>
						<li><a href="<?php echo site_url('login/out');?>">Logout</a></li>
						<?php } else { ?>
						<li<?php if(isset($title) && $title == 'register') echo ' class="active"';?>><a href="<?php echo site_url('register');?>">Register</a></li>
						<li class="divider-vertical"></li>
						<li class="dropdown">
							<a class="dropdown-toggle" href="#" data-toggle="dropdown" data-target="#">Login <i class="caret"></i></a>
							<div class="dropdown-menu">
								<form accept-charset="utf-8" action="<?php echo site_url('login');?>" method="post" style="margin: 0;">
									<fieldset class="dropfield">
										<span class="help-block">Login to view your profile...</span>
										<input type="text" name="username" placeholder="Username..." size="30" required>
										<input type="password" name="password" placeholder="Password..." size="30" required>
										<input class="btn btn-primary pull-right" type="submit" value="Login">
									</fieldset>
								</form>
							</div>
						</li>
						<?php } ?>
					</ul>
				</div>
			</div>
		</div>
