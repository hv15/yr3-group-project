		<div id="push"><!--//--></div>
	</div>
	<footer>
		<div class="container-fluid">
			<div id="footer_1" class="container">
				<div class="row">
					<div class="span1">
						<ul class="nav nav-stacked">
							<li><a href="<?php echo site_url('home');?>">Home</a></li>
							<li><a href="<?php echo site_url('about');?>">About</a></li>
							<li><a href="<?php echo site_url('event');?>">Events</a></li>
							<li><a href="<?php echo site_url('profile');?>">Profile</a></li>
						</ul>
					</div>
					<div class="span2">
						<ul class="nav nav-stacked">
							<li><a href="<?php echo site_url('terms#terms');?>">Terms and Conditions</a></li>
							<li><a href="<?php echo site_url('terms#cookies');?>">Cookie Policy</a></li>
						</ul>
					</div>
					<div class="span4 pull-right text-right">
						<p><a href="<?php echo site_url('about');?>">Riccarton Sports Centre</a> &copy; 2013.</p>
						<p>Created by <a href="//4wardthinking.info">4wardThinking</a>.</p>
					</div>
				</div>
			</div>
		</div>
	</footer>
</body>
</html>
