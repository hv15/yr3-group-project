		<div class="container">
			<div class="row">
				<div class="page-header">
					<h1>Who's who? <small>Information about teams and competitors can be found here</small></h1>
				</div>	
			</div>
			<div class="row">
				<div class="span6">
					<h3>Teams</h3>
					<div class="well">
						<ul class="unstyled">
							<?php if(isset($teams)) foreach($teams as $team){ echo "<li><a href=\"".site_url("profile/{$team['username']}")."\">{$team['team']}</a></li>";} ?>
						</ul>
					</div>
				</div>
				<div class="span6">
					<h3>Competitors</h3>
					<div class="well">
						<ul class="unstyled">
							<?php if(isset($competitors)) foreach($competitors as $competitor){ echo "<li><a href=\"".site_url("profile/{$competitor['username']}")."\">{$competitor['firstname']} {$competitor['lastname']}</a></li>";} ?>
						</ul>
					</div>
				</div>
			</div>
		</div>