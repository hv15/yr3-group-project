		<div class="container">
			<div class="row">
				<div class="page-header">
					<h1>Registration <small>for Competitors and Teams</small></h1>
				</div>	
			</div>
			<div class="row">
				<div id="alertter"></div>
				<div id="forms" class="tabbable tabs-left">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#compreg" data-toggle="tab">Competitors</a></li>
						<li><a href="#teamreg" data-toggle="tab">Teams</a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="compreg">
							<form id="compform" class="form-horizontal regi" action="<?php echo site_url('login/register/comp');?>">
								<div class="control-group">
									<label class="control-label" for="username">Username</label>
									<div class="controls">
										<input type="text" data-validation-ajax-ajax="<?php echo site_url('login/exist');?>" name="username" id="username" placeholder="e.g. bob123" required>
										<span class="required">*</span>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="email">Email</label>
									<div class="controls">
										<input type="email" name="email" id="email" placeholder="e.g. bob@example.com" required>
										<span class="required">*</span>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="password">Password</label>
									<div class="controls">
										<input type="password" name="password" id="password" placeholder="Password" required>
										<span class="required">*</span>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="cfpassword">Repeat your Password</label>
									<div class="controls">
										<input type="password" name="cfpassword" id="cfpassword" data-validation-match-match="password" placeholder="Password" required>
										<span class="required">*</span>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="fname">First Name</label>
									<div class="controls">
										<input type="text" name="fname" id="fname" placeholder="e.g. William" required>
										<span class="required">*</span>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="sname">Surname</label>
									<div class="controls">
										<input type="text" name="sname" id="sname" placeholder="e.g. Smith" required>
										<span class="required">*</span>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="dob">Date of Birth</label>
									<div class="controls">
										<input type="date" name="dob" id="dob" data-date-format="yyyy-mm-dd" placeholder="e.g 1991-01-28" required>
										<span class="required">*</span>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="address">Address</label>
									<div class="controls">
										<textarea rows="3" name="address" id="address" placeholder="e.g 1991-01-28" required></textarea>
										<span class="required">*</span>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="telephone">Telephone</label>
									<div class="controls">
										<input type="text" data-validation-regex-regex="\+?[0-9]{3,}" data-validation-regex-message="Format: (+)0123456789"  id="telephone" name="telephone" placeholder="Telephone" required/>
										<span class="required">*</span>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="sport">Sport</label>
									<div class="controls">
										<select name="sport" id="sport" required>
											<?php if(isset($sports)) foreach($sports as $sport) echo "<option>$sport</option>";?>
										</select>
										<span class="required">*</span>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="team">Team</label>
									<div class="controls">
										<select name="team" id="team" required>
											<option value="NULL">None</option>
											<?php if(isset($teams)) foreach($teams as $team) echo "<option>$team</option>";?>
										</select>
										<span class="required">*</span>
									</div>
								</div>
								<div class="control-group">
									<div class="controls">
										<button type="reset" class="btn btn-info">Clear Form</button>
										<button type="submit" class="btn btn-primary">Register</button>
									</div>
								</div>
							</form>
						</div>
						<div class="tab-pane" id="teamreg">
							<form id="teamform" class="form-horizontal regi" action="<?php echo site_url('login/register/team');?>">
								<div class="control-group">
									<label class="control-label" for="username">Username</label>
									<div class="controls">
										<input type="text" data-validation-ajax-ajax="<?php echo site_url('login/exist');?>" name="username" id="username" placeholder="e.g. bob123" required>
										<span class="required">*</span>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="tname">Team Name</label>
									<div class="controls">
										<input type="text" name="tname" id="tname" placeholder="e.g. The Rangers" required>
										<span class="required">*</span>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="email">Email</label>
									<div class="controls">
										<input type="email" name="email" id="email" placeholder="e.g. bob@example.com" required>
										<span class="required">*</span>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="password">Password</label>
									<div class="controls">
										<input type="password" name="password" id="password" placeholder="Password" required>
										<span class="required">*</span>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="cfpassword">Repeat your Password</label>
									<div class="controls">
										<input type="password" name="cfpassword" id="cfpassword" data-validation-match-match="password" placeholder="Password" required>
										<span class="required">*</span>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="fname">First Name</label>
									<div class="controls">
										<input type="text" name="fname" id="fname" placeholder="e.g. William" required>
										<span class="required">*</span>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="sname">Surname</label>
									<div class="controls">
										<input type="text" name="sname" id="sname" placeholder="e.g. Smith" required>
										<span class="required">*</span>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="address">Address</label>
									<div class="controls">
										<textarea rows="3" name="address" id="address" required></textarea>
										<span class="required">*</span>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="telephone">Telephone</label>
									<div class="controls">
										<input type="text" data-validation-regex-regex="\+?[0-9]{3,}" data-validation-regex-message="Format: (+)0123456789"  id="telephone" name="telephone" placeholder="Telephone" required/>
										<span class="required">*</span>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="memnum">Number of Team Members</label>
									<div class="controls">
										<input type="text" id="memnum" name="memnum" placeholder="e.g. 11" required/>
										<span class="required">*</span>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="membership">Sport Assocciation</label>
									<div class="controls">
										<input type="text" id="membership" name="membership" placeholder="e.g. National Wattball Association"/>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="membershipnum">Sport Assocciation Number</label>
									<div class="controls">
										<input type="text" id="membershipnum" name="membershipnum" placeholder="e.g. NWA001"/>
									</div>
								</div>
								<div class="control-group">
									<div class="controls">
										<button type="reset" class="btn btn-info">Clear Form</button>
										<button type="submit" class="btn btn-primary">Register</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>			
			</div>
		</div>
		<script type="text/javascript">
			head.js(
				{'editable': '<?php echo base_url('js/bootstrap-editable.min.js');?>'},
				{'validate': '<?php echo base_url('js/jqBootstrapValidation.js');?>'},
				'<?php echo base_url('css/bootstrap-editable.css');?>'
			);
			
			head.ready(function(){
			
				$('#dob').datepicker({
					'startView': 2
				});
				
				$(function(){
					$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
				});
				
				$('.regi').submit(function(){
					console.log("here");
					$.post(this.action, $(this).serialize(), function(response, data, jqXHR){
						if(jqXHR.status == 200){
							$('#forms').hide();
							$('#alertter').append('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button><h5 class="alert-heading">Registered Successfully</h5><p><em>You can now Login</em></p><p><a class="btn btn-primary" href="<?php echo site_url('home');?>">Cool!</a><a class="btn btn-warning" href="#">Register another account</a></p></div>');
						} else {
							$('#forms').hide();
							$('#alertter').append('<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">&times;</button><h5 class="alert-heading">Something went wrong...</h5><p><em>You were not registered</em></p><p><a class="btn btn-warning" href="#">Try Again</a></p></div>');
						}
					});
					return false;
				});
			
				$('#alertter').on('click', '.btn-warning', function(){
					$('#alertter').alert('close');
					$('#forms').show();
				});
			});
		</script>
