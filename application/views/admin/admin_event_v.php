		<div class="container">
			<div class="row">
				<div class="span12">
					<div class="page-header">
						<h3>Events <small>Admin Panel</small></h3>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="span2">
					<div class="well" style="padding: 4px 0;">
						<ul class="nav nav-list">
							<li><a href="<?php echo site_url('admin');?>">Admin Home</a></li>
							<li><a href="<?php echo site_url('admin/staff');?>">View Staff</a></li>
							<li><a href="<?php echo site_url('admin/competitors');?>">View Competitors</a></li>
							<li><a href="<?php echo site_url('admin/teams');?>">View Teams</a></li>
							<li class="active"><a href="<?php echo current_url();?>">View Events</a></li>
							<li data-target="#misc_menu" data-toggle="collapse">
								<span class="nav-header">Misc.<i class="icon-chevron-down pull-right"></i></span>
								<ul class="nav nav-list collapse" id="misc_menu">
									<li><a href="<?php echo site_url('admin/users');?>">View Users</a></li>
									<li><a href="<?php echo site_url('admin/sports');?>">View Sports</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
				<div class="span10">
					<table id="Events" border="0" cellpadding="0" cellspacing="0" class="table table-striped table-bordered datatable">
						<thead>
							<tr>
								<th></th>
								<th>Title</th>
								<th>Start</th>
								<th>End</th>
								<th>Sport</th>
								<th>Umpire</th>
								<th>Location</th>
							</tr>
						</thead>
						<tbody></tbody>
						<tfoot>
							<tr>
								<td></td>
								<td><input type="text" name="search_sport" value="Search position" class="search_init" /></td>
								<td><input type="text" name="search_start" value="Search start" class="search_init" /></td>
								<td><input type="text" name="search_end" value="Search end" class="search_init" /></td>
								<td><input type="text" name="search_sport" value="Search sport" class="search_init" /></td>
								<td><input type="text" name="search_umpire" value="Search umpire" class="search_init" /></td>
								<td><input type="text" name="search_location" value="Search location" class="search_init" /></td>
							</tr>
						</tfoot>
					</table>
					<!-- Modal ADD EVENT -->
					<div id="add_row_modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="add_row_modal" aria-hidden="true">
						<form id="add_row_form" class="modal-form form-horizontal" action="<?php echo site_url('admin/events/add');?>">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i></button>
								<h3 id="add_row_modal_label">Add Row to Table</h3>
							</div>
							<div class="modal-body">
								<div class="alert alert-error fade">
									<strong>Error!</strong> Transaction was not completed!
								</div>
								<div class="control-group">
									<label class="control-label" for="title">Event Title</label>
									<div class="controls">
										<input type="text" id="title" name="title" required/>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="start">Start Time</label>
									<div class="controls">
										<input type="text" id="start" name="start" required/>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="end">End Time</label>
									<div class="controls">
										<input type="text" id="end" name="end" required/>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="sport">Sport</label>
									<div class="controls">
										<select name="sport" id="sport" required>
											<?php if(isset($sports)) foreach($sports as $sport) echo "<option>$sport</option>";?>
										</select>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="umpire">Umpire</label>
									<div class="controls">
										<input type="text" id="umpire" name="umpire" required/>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="location">Location</label>
									<div class="controls">
										<input type="text" id="location" name="location" required/>
									</div>
								</div>
							</div>
							<div class="modal-footer">
								<button id="reset" type="reset" class="btn">Reset</button>
								<button id="submit" type="submit" class="btn btn-primary">Save</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<script type="text/javascript" charset="utf-8">
			head.js(
				{'editable': '<?php echo base_url('js/bootstrap-editable.min.js');?>'},
				{'datatables': '<?php echo base_url('js/jquery.dataTables.min.js');?>'},
				{'validate': '<?php echo base_url('js/jqBootstrapValidation.js');?>'},
				{'moment' : '<?php echo base_url('js/moment.min.js');?>'},
				'<?php echo base_url('css/bootstrap-editable.css');?>',
				'<?php echo base_url('css/jquery.dataTables.css');?>'
			);
			var asInitVals = new Array();
			head.ready(function(){
				$.fn.editable.defaults.mode = 'inline';
				var datatable = $('#Events').dataTable({
					'sDom': "<'row'<'span5'<'#add_row'l>><'span5'f>r><'row'<'span10'<'#alertter'>>><'row'<'span10't>><'row'<'span5'i><'span5'p>>",
					'oLanguage': {
						'sSearch': 'Search all columns:'
					},
					'bProcessing': true,
					'bServerSide': true,
					'bAutoWidth': false,
					'bScrollCollapse': false,
					'sScrollY': '250px',
					'sScrollX': '780px',
					'sAjaxSource': '<?php echo site_url('admin/table/matches');?>',
					'bScrollInfinite': true,
					'bPaginate': true, // needs to be set for infinite scrolling to work
					'sPaginationType': 'bootstrap',
					'fnDrawCallback': function(){
						$('#Events tbody td .edita').editable({
							'disabled': true,
							'success': function(response, newVal){
								if(response)
									datatable.fnDraw();
								else
									return "Record could not be updated!";
							}
						});
					},
					'aoColumns': [
						{
							'mData': null,
							'mRender': function(data, type, ellse){
								return '<a data-toggle="modal" data-pk="'+ellse.id+'" href="#" class="btn btn-danger delete_row"><i class="icon-trash"></i> Delete</a><a data-pk="'+ellse.id+'" href="#" class="btn btn-info edit_row"><i class="icon-pencil"></i> Edit</a>';
							},
							'bSortable': false,
							'bSearchable': false
						},
						{
							'mData': "title",
							'mRender': function(data, type, ellse){
								return '<a href="#" id="title" data-type="text" data-pk="'+ellse.id+'" data-url="<?php echo site_url('admin/events/edit');?>" class="edita">'+data+'</a>';
							}
						},
						{
							'mData': "start",
							'mRender': function(data, type, ellse){
								return '<a href="#" id="start" data-type="combodate" data-mode="popup" data-template="YYYY-MM-DD HH:mm:ss" data-format="YYYY-MM-DD HH:mm:ss" data-pk="'+ellse.id+'" data-url="<?php echo site_url('admin/events/edit');?>" class="edita">'+data+'</a>';
							}
						},						
						{
							'mData': "end",
							'mRender': function(data, type, ellse){
								return '<a href="#" id="end" data-type="combodate" data-mode="popup" data-template="YYYY-MM-DD HH:mm:ss" data-format="YYYY-MM-DD HH:mm:ss" data-pk="'+ellse.id+'" data-url="<?php echo site_url('admin/events/edit');?>" class="edita">'+data+'</a>';
							}
						},						
						{
							'mData': "sport",
							'mRender': function(data, type, ellse){
								return '<a href="#" id="sport" data-type="text" data-pk="'+ellse.id+'" data-url="<?php echo site_url('admin/events/edit');?>" class="edita">'+data+'</a>';
							}
						},						
						{
							'mData': "umpire",
							'mRender': function(data, type, ellse){
								return '<a href="#" id="umpire" data-type="text" data-pk="'+ellse.id+'" data-url="<?php echo site_url('admin/events/edit');?>" class="edita">'+data+'</a>';
							}
						},						
						{
							'mData': "location",
							'mRender': function(data, type, ellse){
								return '<a href="#" id="location" data-type="text" data-pk="'+ellse.id+'" data-url="<?php echo site_url('admin/events/edit');?>" class="edita">'+data+'</a>';
							}
						}
					]
				});

				$('.datatable tfoot input').keyup(function(){
					datatable.fnFilter(this.value, $('.datatable tfoot input').index(this) + 1);
				});				

				$('.datatable tfoot input').each(function(i){
					asInitVals[i] = this.value;
				});				

				$('.datatable tfoot input').focus(function(){
					if(this.className == 'search_init')
					{
						this.className = '';
						this.value = '';
					}
				});				

				$('.datatable tfoot input').blur(function(i){
					if(this.value == '')
					{
						this.className = 'search_init';
						this.value = asInitVals[$('.datatable tfoot input').index(this)];
					}
				});
				
				$('#Events').on('click','.edit_row',function(e){
					e.stopPropagation();
					$(this).button('toggle');
					console.log($(this));
					$('.edita[data-pk='+$(this).attr('data-pk')+']').editable('toggleDisabled');
				});
				
				$('#Events').on('click', '.delete_row', function(){
					var pk = $(this).attr('data-pk');
					$('#alertter').append('<div class="alert alert-info '+pk+'"><button type="button" class="close" data-dismiss="alert">&times;</button><h5 class="alert-heading">Are you sure you want to delete '+pk+'?</h5><p><em>This deletion will be permanent and irreversible!</em></p><p><a class="btn btn-danger" href="#">Yes, Delete</a><a class="btn btn-primary" href="#">No</a></p></div>');
					$('#alertter .'+pk+'').on('click','.btn-danger', function(){
						$.post('<?php echo site_url('admin/events/del');?>', {'pk': pk})
							.done(function(data){
								$('#alertter .'+pk+'').remove();
								datatable.fnDraw();
							})
							.fail(function(xhr, textStatus, errorThrown){
								$('#alertter .'+pk+'').remove();
								$('#alertter').append('<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Delete Failed!</strong> Position '+pk+' could not be deleted...</div>');
							});
					});
					$('#alertter .'+pk+'').on('click','.btn-primary', function(){
						$('#alertter .'+pk+'').remove();
					});
				});

				$(function(){
					$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
				});
				
				$('#add_row').html('<a href="#add_row_modal" role="button" class="btn btn-info" data-toggle="modal"><i class="icon-plus-sign"></i> Add Row</a>');
				
				$('#add_row_form #reset').click(function(){
					$('#add_row_modal .alert-error').removeClass('in');
				});
				
				$('#add_row_form #start').combodate({
					format: 'YYYY-MM-DD HH:mm:ss',
					template: 'YYYY-MMM-DD HH:mm:ss',
				});
				
				$('#add_row_form #end').combodate({
					format: 'YYYY-MM-DD HH:mm:ss',
					template: 'YYYY-MMM-DD HH:mm:ss',
				});

				$('#add_row_form').submit(function(){
					$.post(this.action, $(this).serialize(), function(response, data, jqXHR){
						if(jqXHR.status == 200){
							$('#add_row_modal').modal('hide');
							$('#add_row_modal').on('hidden',function(){
								// reset not working...
								//$('#add_row_form').each(function(){ this.reset(); });
								datatable.fnDraw();
							});
						} else {
							// this can be done better!
							$('#add_row_modal .alert-error').addClass('in');
						}
					});
					return false;
				});
			});
		</script>