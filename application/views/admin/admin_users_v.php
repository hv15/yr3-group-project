		<div class="container">
			<div class="row">
				<div class="span12">
					<div class="page-header">
						<h3>Users <small>Admin Panel</small></h3>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="span2">
					<div class="well" style="padding: 4px 0;">
						<ul class="nav nav-list">
							<li><a href="<?php echo site_url('admin');?>">Admin Home</a></li>
							<li><a href="<?php echo site_url('admin/staff');?>">View Staff</a></li>
							<li><a href="<?php echo site_url('admin/competitors');?>">View Competitors</a></li>
							<li><a href="<?php echo site_url('admin/teams');?>">View Teams</a></li>
							<li><a href="<?php echo site_url('admin/events');?>">View Events</a></li>
							<li data-target="#misc_menu" data-toggle="collapse">
								<span class="nav-header">Misc.<i class="icon-chevron-down pull-right"></i></span>
								<ul class="nav nav-list collapse in" id="misc_menu">
									<li class="active"><a href="<?php echo current_url();?>">View Users</a></li>
									<li><a href="<?php echo site_url('admin/sports');?>">View Sports</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
				<div class="span10">
					<table id="Users" border="0" cellpadding="0" cellspacing="0" class="table table-striped table-bordered datatable">
						<thead>
							<tr>
								<th></th>
								<th>Username</th>
								<th>Role</th>
								<th>Hidden</th>
							</tr>
						</thead>
						<tbody></tbody>
						<tfoot>
							<tr>
								<td></td>
								<td><input type="text" name="search_username" value="Search username" class="search_init" /></td>
								<td><input type="text" name="search_role" value="Search role" class="search_init" /></td>
								<td><input type="text" name="search_hidden" value="Search hidden" class="search_init" /></td>
							</tr>
						</tfoot>
					</table>
					<!-- Modal CHANGE PASSWORD -->
					<div id="change_pw_modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="change_pw_modal" aria-hidden="true">
						<form id="change_pw_form" class="modal-form form-horizontal" action="<?php echo site_url('admin/users/pass');?>">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								<h3 id="add_row_modal_label">Change User Password</h3>
							</div>
							<div class="modal-body">
								<div class="alert alert-error fade">
									<strong>Error!</strong> Transaction was not completed!
								</div>
								<div class="control-group">
									<label class="control-label" for="username">Username</label>
									<div class="controls">
										<input type="text" id="username" name="username" placeholder="Username" readonly/>
										<p class="help-block"></p>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="password">Password</label>
									<div class="controls">
										<input type="password" id="password" name="password" maxlength="25" placeholder="Password" required/>
										<p class="help-block"></p>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="passwordr">Lastname</label>
									<div class="controls">
										<input type="password" data-validation-match-match="password" id="passwordr" name="passwordr" maxlength="25" placeholder="Repeat" required/>
										<p class="help-block"></p>
									</div>
								</div>
							</div>
							<div class="modal-footer">
								<button id="reset" type="reset" class="btn">Reset</button>
								<button id="submit" type="submit" class="btn btn-primary">Save</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<script type="text/javascript" charset="utf-8">
			head.js(
				{'editable': '<?php echo base_url('js/bootstrap-editable.min.js');?>'},
				{'datatables': '<?php echo base_url('js/jquery.dataTables.min.js');?>'},
				{'validate': '<?php echo base_url('js/jqBootstrapValidation.js');?>'},
				'<?php echo base_url('css/bootstrap-editable.css');?>',
				'<?php echo base_url('css/jquery.dataTables.css');?>'
			);
			var asInitVals = new Array();
			head.ready(function(){
				$.fn.editable.defaults.mode = 'inline';
				var datatable = $('#Users').dataTable({
					'sDom': "<'row'<'span5'<'#add_row'l>><'span5'f>r><'row'<'span10'<'#alertter'>>><'row'<'span10't>><'row'<'span5'i><'span5'p>>",
					'oLanguage': {
						'sSearch': 'Search all columns:'
					},
					'bProcessing': true,
					'bServerSide': true,
					'bAutoWidth': false,
					'bScrollCollapse': false,
					'sScrollY': '250px',
					'sScrollX': '780px',
					'sAjaxSource': '<?php echo site_url('admin/table/users');?>',
					'bScrollInfinite': true,
					'bPaginate': true, // needs to be set for infinite scrolling to work
					'sPaginationType': 'bootstrap',
					'fnDrawCallback': function(){
						$('#Users tbody td .edita').editable({
							'disabled': true,
							'success': function(response, newVal){
								if(response)
									datatable.fnDraw();
								else
									return "Record could not be updated!";
							}
						});
					},
					'aoColumns': [
						{
							'mData': null,
							'mRender': function(data, type, ellse){
								return '<a data-toggle="modal" data-pk="'+ellse.username+'" href="#" class="btn btn-danger delete_row"><i class="icon-trash"></i> Delete</a><a data-toggle="modal" data-pk="'+ellse.username+'" href="#change_pw_modal" class="btn btn-primary change_pw"><i class="icon-lock"></i> Change Password</a><a data-pk="'+ellse.username+'" href="#" class="btn btn-info edit_row"><i class="icon-pencil"></i> Edit</a>';
							},
							'bSortable': false,
							'bSearchable': false
						},
						{
							'mData': 'username',
							'mRender': function(data, type, ellse){
								return '<a href="#" id="username" maxlength="15" data-type="text" data-pk="'+ellse.username+'" data-url="<?php echo site_url('admin/users/edit');?>">'+data+'</a>';
							}
						},
						{
							'mData': 'role',
							'mRender': function(data, type, ellse){
								return '<a href="#" id="role" data-type="text" data-pk="'+ellse.username+'" data-url="<?php echo site_url('admin/users/edit');?>">'+data+'</a>';
							}
						},
						{
							'mData': 'hidden',
							'mRender': function(data, type, ellse){
								return '<a href="#" id="hidden" data-type="text" data-pk="'+ellse.username+'" data-url="<?php echo site_url('admin/users/hidden');?>" class="edita">'+data+'</a>';
							}
						}
					]
				});

				$('.datatable tfoot input').keyup(function(){
					datatable.fnFilter(this.value, $('.datatable tfoot input').index(this) + 1);
				});				

				$('.datatable tfoot input').each(function(i){
					asInitVals[i] = this.value;
				});				

				$('.datatable tfoot input').focus(function(){
					if(this.className == 'search_init')
					{
						this.className = '';
						this.value = '';
					}
				});				

				$('.datatable tfoot input').blur(function(i){
					if(this.value == '')
					{
						this.className = 'search_init';
						this.value = asInitVals[$('.datatable tfoot input').index(this)];
					}
				});
				
				$('#Users').on('click','.edit_row',function(e){
					e.stopPropagation();
					$(this).button('toggle');
					$('.edita[data-pk='+$(this).attr('data-pk')+']').editable('toggleDisabled');
				});
				
				$('#Users').on('click', '.delete_row', function(){
					var pk = $(this).attr('data-pk');
					$('#alertter').append('<div class="alert alert-info '+pk+'"><button type="button" class="close" data-dismiss="alert">&times;</button><h5 class="alert-heading">Are you sure you want to delete '+pk+'?</h5><p><em>This deletion will be permanent and irreversible!</em></p><p><a class="btn btn-danger" href="#">Yes, Delete</a><a class="btn btn-primary" href="#">No</a></p></div>');
					/*$('#alertter .'+pk+'').on('click','.btn-danger', function(){
						$.post('<?php echo site_url('admin/staff/del');?>', {'pk': pk}, function(data, textStatus, jqXHR){
							if(jqXHR.status == 200){
								$('#alertter .'+pk+'').remove();
								datatable.fnDraw();
							} else
								$('#alertter').append('<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Delete Failed!</strong> User '+pk+' could not be deleted...</div>');
						});
					});*/
					$('#alertter .'+pk+'').on('click','.btn-primary', function(){
						$('#alertter .'+pk+'').remove();
					});
				});
				
				$('#Users').on('click', '.change_pw', function(){
					$('#change_pw_form #username').attr('value', $(this).attr('data-pk'));
				});

				$(function(){
					$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
				});
				
				$('#change_pw_form').submit(function(){
					$.post(this.action, $(this).serialize(), function(response, data, jqXHR){
						if(jqXHR.status == 200){
							$('#change_pw_modal').modal('hide');
							$('#change_pw_modal').on('hidden',function(){
								// reset not working...
								//$('#add_row_form').each(function(){ this.reset(); });
								datatable.fnDraw();
							});
						} else {
							// this can be done better!
							$('#change_pw_modal .alert-error').addClass('in');
						}
					});
					return false;
				});
			});
		</script>