		<div class="container">
			<div class="row">
				<div class="span12">
					<div class="page-header">
						<h3>Home <small>Admin Panel</small></h3>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="span2">
					<div class="well" style="padding: 4px 0;">
						<ul class="nav nav-list">
							<li class="active"><a href="<?php echo current_url();?>">Admin Home</a></li>
							<li><a href="<?php echo site_url('admin/staff');?>">View Staff</a></li>
							<li><a href="<?php echo site_url('admin/competitors');?>">View Competitors</a></li>
							<li><a href="<?php echo site_url('admin/teams');?>">View Teams</a></li>
							<li><a href="<?php echo site_url('admin/events');?>">View Events</a></li>
							<li data-target="#misc_menu" data-toggle="collapse">
								<span class="nav-header">Misc.<i class="icon-chevron-down pull-right"></i></span>
								<ul class="nav nav-list collapse" id="misc_menu">
									<li><a href="<?php echo site_url('admin/users');?>">View Users</a></li>
									<li><a href="<?php echo site_url('admin/sports');?>">View Sports</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
				<div class="span10">
					<p>Welcome to the admin panel</p>
				</div>
			</div>
		</div>