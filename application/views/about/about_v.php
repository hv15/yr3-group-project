		<div class="container">
			<div class="row">
				<div class="span12">
					<div class="page-header">
						<h1>What are we all About? <small>We are a respected provider of facilities for various sporting activites</small></h1>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="span3">
					<address>
						<strong>Riccarton Sport Centre</strong><br>
						Heriot Watt University <br>
						Edinburgh EH14 1AS<br>
						United Kingdom<br>
						0131 449 5111
					</address>
				</div>
				<div id="map_wrap" class="span9">
					<div id="map_canvas"></div>
				</div>
			</div>
			<div class="row">
				<div class="span12">
					<dl class="dl-horizontal">
						<dt>Sports</dt>
						<dd>Tennis, Badminton, Football, Rugby, Basketball, Field & Track, Wattball, and many more...</dd>
						<dt>Facilities</dt>
						<dd>Fully equipted gym, inside pitches and courst, outside fields and tracks</dd>
					</dl>
				</div>
			</div>
		</div>
		<script type="text/javascript" charset="utf-8">
			head.js(
				{'maps': 'https://maps.googleapis.com/maps/api/js?key=AIzaSyCBPGSxVqeV9pTZYvPw8D_wKfCLTUy8Wpk&sensor=false&callback=initialize'}
			);
			function initialize() {	
				var mapOptions = {
					center: new google.maps.LatLng(55.908809, -3.320103),
					zoom: 15,
					mapTypeId: google.maps.MapTypeId.ROADMAP
				};
				var map = new google.maps.Map(document.getElementById('map_canvas'),mapOptions);
				
				var marker = new google.maps.Marker({
					position: new google.maps.LatLng(55.908981, -3.31819),
					map: map,
					title:"Riccarton Sport Centre"
				});
				google.maps.event.addListener(marker, 'click', toggleBounce);
				function toggleBounce() {
					if (marker.getAnimation() != null) {
						marker.setAnimation(null);
					} 
					else {
						marker.setAnimation(google.maps.Animation.BOUNCE);
					}
				}
			}
		</script>
