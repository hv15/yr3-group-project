		<div class="container">
			<div class="row-fluid">
				<div class="hero-unit">
					<h1>Sporting for Life!</h1>
					<p>Get your game on, and join in!</p>
				</div>	
			</div>
			<div class="row-fluid">
				<div class="span4 well">
					<h4>Local Weather</h4>
					<a href="http://www.wunderground.com/cgi-bin/findweather/getForecast?query=zmw:00000.25.03160&bannertypeclick=wu_macwhite"><img src="http://weathersticker.wunderground.com/cgi-bin/banner/ban/wxBanner?bannertype=wu_macwhite_metric&airportcode=EGPH&ForcedCity=Edinburgh&ForcedState=" alt="Click for Edinburgh, United Kingdom Forecast" height="90" width="160" /></a>
				</div>
				<div class="span8 well">
					<h4>Upcoming Events</h4>
					<table class="table table-striped">
					<thead>
						<tr>
							<th>Match</th>
							<th>Date and Time</th>
							<th>Location</th>
						</tr>
					</thead>
					<?php
						if(isset($events))
						{
							foreach($events as $event)
							{
								if(strtotime('+0 day', strtotime($event['start'])) < strtotime($event['end']))
								{
									$start = date("d M H:i", strtotime($event['start']));
									$end = date("d M H:i", strtotime($event['end']));
								}
								else
								{
									$start = date("d M H:i", strtotime($event['start']));
									$end = date("H:i", strtotime($event['end']));
								}
								echo "	<tr><td>{$event['title']}</td><td>$start - $end</td><td>{$event['location']}</td></tr>";
							}
						}
					?>
					</table>
				</div>
			</div>
		</div>