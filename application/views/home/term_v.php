		<div class="container">
			<div class="row">
				<div id="scroller" class="span2">
					<ul class="nav nav-pills nav-stacked">
						<li class="active"><a href="#terms">Terms and Conditions</a></li>
						<li><a href="#cookies">Cookie Policy</a></li>
					</ul>
				</div>
				<div id="term_content" class="span10">
					<h3 id="terms">Terms and Conditions</h3>
					<h4>General</h4>
					<p>This website is owned and operated by Riccarton Sports Centre, whose registered office is at Riccarton Sports Centre, Edinburgh Campus, EH14 4AS, Edinburgh, Scotland.</p>
					<p>For the purpose of these Terms and Conditions:  “We”, “Our” and “Us” refers to Riccarton Sports Centre while references to “You” and “Your” refers to the persons accessing this website (including persons who access this site on the behalf of other persons); references to “Material” or “Materials” includes data, information and databases.</p>
					<p>Please review these Terms and Conditions carefully before using this website. Your use of this website indicates your agreement to be bound by these Terms and Conditions in consideration of the access provided to the website as detailed below.</p>
					<p>If you do not agree to these Terms and Conditions, any of the related documentation linked below or other restrictions notified to you during the course of your use of this website you are not permitted to, and agree not to, use or access this website.</p>
					<h4>Data protection and cookies</h4>
					<p>Any personal information you supply to us when you use this website will be used in accordance with Our Privacy Policy. Information about Cookies and the use of Cookies on this website is provided on our Cookies page.</p>
					<h4>Proprietary rights</h4>
					<p>All materials on this website, the appearance, organisation and layout of this website, the underlying software code and the underlying data are subject to trade marks, copyright, database rights and other registered and unregistered intellectual property rights which are owned either directly by Us or by Our licensors.</p>
					<p>Unless otherwise authorised within these Terms and Conditions, You must not copy, modify, alter, publish, broadcast, distribute, sell or transfer (whether in whole or in part) any material on this website or the underlying software code or underlying data.</p>
					<h4>Permitted users of this website</h4>
					<p>Your use of this website is personal to you.  We provide this website solely to permit you to view the availability of our goods and services and to transact business with us and for no other purpose.</p>
					<p>You are responsible for maintaining the confidentiality of your website access information (such as usernames, passwords and personal identification numbers) and booking data (such as booking references). You may choose to permit family members, friends or colleagues to access the website on your behalf by providing website access information and/or booking data to such persons provided that:</p>
					<ul>
						<li>You have obtained the consent of any other persons whose booking data or personal data is accessible with your data;</li>
						<li>You accept full responsibility for the acts and omissions of that person when accessing the website using the website access information and/or booking data  provided by you whether such access is authorised by You or not.</li>
						<li>You must not permit access to or use of this website or any of the Material on or obtained from this website to any commercial entity or other organisation providing a service to the public.</li>
					</ul>
					<h4>Security</h4>
					<p>If You have reason to believe that Your access to the website is no longer secure (e.g. loss, theft or unauthorized disclosure or use of user names, passwords and personal identification numbers), You must promptly change the affected access information.</p>
					<h4>Appropriate use</h4>
					<p>Your access to this website is on a temporary basis, and we reserve the right to withdraw, restrict or amend the services accessible to You on the website without notice at our absolute discretion. You may only use this website in accordance with these Terms and Conditions and, in any event, for lawful and proper purposes which includes complying with all applicable laws, regulations and codes of practice within the UK or other jurisdiction from which You are accessing this website.  Except as expressly permitted by Us, You shall not use this website for any purpose.</p>
					<p>In particular, You agree that, You must not:</p>
					<ul>
						<li>post, transmit or disseminate any information on or via this website which is or may be harmful, obscene, defamatory or otherwise illegal;</li>
						<li>use ‘screen scraping’, any automated algorithm, device, method, system, software or manual process to access, use, search, copy, monitor or extract Material (in whole or in part) from or through using this website unless We have given Our express written agreement;</li>
						<li>use this website in a manner which causes or may cause an infringement of the rights of any other</li>
						<li>make any unauthorised, false or fraudulent booking</li>
						<li>use any software, routine or device to interfere or attempt to interfere electronically or manually with the operation or functionality of this website including but not limited to uploading or making available files containing corrupt data or viruses via whatever means</li>
						<li>deface, alter or interfere with the front end ‘look and feel’ of this website or the underlying software code</li>
						<li>take any action that imposes an unreasonable or disproportionately large load on this website or related infrastructure</li>
						<li>permit any automatic registration, logging in or access to any of the Material on the website</li>
						<li>obtain or attempt to obtain unauthorised access, via whatever means, to any of Our networks or accounts or to information about other users</li>
						<li>disclose confidential information of any person or entity including without limitation Our confidential information.</li>
					</ul>
					<p>Without prejudice to any of Our other rights (whether at law or otherwise) We reserve the right to:</p>
					<ul>
						<li>cancel Your bookings and purchases without reference to You; and/or</li>
						<li>deny You access to this website and block your access to the website</li>
						<li>where We believe (in Our absolute discretion) that You (or persons who access this site on your behalf using website access information and/or booking data that You have provided to them) are in breach of any of these Terms and Conditions.</li>
					</ul>
					<h4>Changes to this website</h4>
					<p>We may make improvements or changes to the information, services, products and other Materials on this website, or terminate this website, at any time without notice. We may also modify these Terms and Conditions at any time, and such modification shall be effective immediately upon posting of the modified Terms and Conditions on this website. Accordingly, Your continued access or use of this website is deemed to be Your acceptance of the modified Terms and Conditions.</p>
					<h4>Limitation of liability</h4>
					<p>In no event will We be liable for any direct, indirect, special, punitive, exemplary or consequential losses or damages of whatsoever kind arising out of access to, or the use of this website or any information contained in it, including loss of profit and the like whether or not in the contemplation of the parties, whether based on breach of contract, tort (including negligence), product liability or otherwise, even if advised of the possibility of such damages.</p>
					<p>Nothing in these Terms and Conditions shall exclude or limit Our liability for death or personal injury caused by negligence or for fraud and fraudulent misrepresentation. All software products downloaded from any section of this website or via a link pointed to by this website are downloaded, installed, and used totally and entirely at the users own risk.</p>
					<h4>Disclaimer of warranty</h4>
					<p>To the maximum extent permitted by law, We disclaim all implied warranties with regard to the information, services and Materials contained on this website. All such information, services and Materials are provided "as is" and "as available" without warranty of any kind.</p>
					<p>We make no warranty whatsoever for the reliability, stability or any virus-free nature of any software being downloaded from this website, nor for the availability of the download sites where applicable.</p>
					<h4>Indemnification</h4>
					<p>You agree to indemnify, defend and hold Us harmless from any liability, loss, claim and expense (including reasonable legal fees) related to Your breach of these Terms and Conditions (including, but not limited to, any breach by persons acting on your behalf who access this site using website access information and/or booking/purchase data that You have provided to them).</p>
					<h4>Miscellaneous</h4>
					<p>These Terms and Conditions contain all the terms of Your agreement with Us relating to Your use of this website. No other written or oral statement (including statements in any brochure or promotional literature published by Us) will be incorporated.</p>
					<p>Any failure to assert any rights We may have under these Terms and Conditions does not constitute a waiver of Our right to assert the same or any other right at any other time or against any other person.</p>
					<p>If any provision of this Agreement is found to be invalid or unenforceable, then the invalid or unenforceable provision will be removed from these Terms and Conditions without affecting the validity or enforceability of any other provision.</p>
					<p>Throughout this website the terms "partner(s)" and "partnership(s)" are used to refer to individual marketing or co-operation agreements and not to any relationship which has specific legal or tax implications. We cannot therefore accept any liability for the conduct of these partner organisations.</p>
					<p>Throughout this website the term "person(s)" is used to refer to natural and legal persons.</p>
					<h3 id="cookies">Cookie Policy</h3>
					<p>This document explains what you need to know about how Riccarton Sports Centre uses cookies on its websites, and how to choose to manage or remove them if you wish to do so.</p>
					<p>Cookies are small data files which are sent to a user’s computer or mobile phone from a website and are stored on the hard drive of the user’s device. They are helpful because they help to make a website work better for a user.</p>
					<p>We may store information about your visit using cookies (files which are sent by us to your computer or other access device) which we can access when you visit our corporate site in future. We do this for a number of reasons, such as:</p>
					<ul>
						<li>recognising your preferences so you don't need to re-enter them on every visit</li>
						<li>tracking progress through online applications</li>
						<li>monitoring site performance and content popularity</li>
						<li>to speed up your searches</li>
						<li>to recognise you when you return to our website</li>
					</ul>
					<h4>Types of Cookies used</h4>
					<h5>Traffic Log Cookies</h5>
					<p>We use traffic log cookies to identify which pages are most being used. This helps us analyse data about web page traffic and improve our website by tailoring it to user needs. We only use this information for statistical analysis purposes, then the data is removed from the system.</p>
					<p>Overall, cookies help us provide you with a better website, by enabling us to monitor which pages you find useful and which you do not. A cookie in no way gives us access to your computer or any information about you, other than the data you choose to share with us.</p>
					<h5>Session cookies</h5>
					<p>Session cookies last only for the duration of your visit and are deleted when you close your browser. These facilitate various tasks such as allowing a website to identify that a user of a particular device is navigating from page to page, supporting website security or basic functionality.</p>
					<p>Many of the cookies we use are session cookies. For example, they help us to ensure the security of your account logging session, and can also keep you logged in to your account while you move between pages or service your account.</p>
					<p>Our session cookies used for security are designed to be very difficult to use except by us when you have an active account session. They contain no personal information that can be used to identify an individual. Their names typically start with the letters IB e.g. IBSESSION, IBCOOKIE01, IBCOOKIE02.</p>
					<h5>Persistent cookies</h5>
					<p>Persistent cookies last after you have closed your browser, and allow a website to remember your actions and preferences. Sometimes persistent cookies are used by websites to provide targeted advertising based upon the browsing history of the device.</p>
					<p>Riccarton Sports Centre uses persistent cookies in a few ways, for example, to remember your username for log in so you don’t have to (cookie named HxUseLoginCookie). We also use persistent cookies to allow us to analyse customer visits to our site, for example our cookie named WT_fpc. These cookies help us to understand how customers arrive at and use our site so we can improve the account management service.</p>
					<h5>Strictly necessary cookies</h5>
					<p>These cookies are essential in order to enable you to move around the website and use its features, and ensuring the security of your account management experience. Without these cookies services you have asked for, such as applying for products and managing your accounts, cannot be provided. These cookies don’t gather information about you for the purposes of marketing.</p>
					<h5>Performance cookies</h5>
					<p>These cookies collect information about how visitors use a web site, for instance which pages visitors go to most often, and if they get error messages from web pages. These cookies don't collect information that identifies a visitor although they may collect the IP address of the device used to access the site. All information these cookies collect is anonymous and is only used to improve how a website works, the user experience and to optimise our advertising. By using our websites you agree that we can place these types of cookies on your device, however you can block these cookies using your browser settings.</p>
					<h5>Functionality cookies</h5>
					<p>These cookies allow the website to remember choices you make (such as your username). They may also be used to provide services you have requested such as watching a video. The information these cookies collect is anonymised (i.e. it does not contain your name, address etc.) and they do not track your browsing activity across other websites. By using our websites you agree that we can place these types of cookies on your device, however you can block these cookies using your browser settings.</p>
					<h5>'First Party' cookies</h5>
					<p>This cookie is essential for the website to function properly. It lasts only for the length of time during which you view the website, and improves your experience by letting the site remember things as you move between pages. For example, if you are logged into the site on one page, this fact is held in your session and you continue to be logged in when you view a different page.</p>
					<h5>'Third Party' cookies</h5>
					<p>YouTube - Sometimes we may display YouTube adverts on the website and this may result in YouTube setting cookies. YouTube's privacy policy can be found here http://www.google.co.uk/intl/en/policies/privacy/</p>
					<h4>Managing Cookies</h4>		
					<p>Internet browsers allow users to manage their cookies. They can set their devices to accept all cookies, to let them know when a cookie has been set, or never to receive cookies.</p>
					<p>Each browser works in a different way: use the ‘Help’ menu in your browser to find out to change your cookie preferences. For information on how to do this on the browser of your mobile phone you should refer to your handset manual.</p>
					<p>Please be aware that disabling cookies completely means that certain personalised features will not then be provided to that user and therefore they may not be able to make full use of all of the website’s features.</p>
				</div>
			</div>
		</div>
		<script type="text/javascript" charset="utf-8">
			head.ready(function(){
				$(window).resize(function(){
					$('#term_content').css('height', window.innerHeight - 190);
				});
				$('#term_content').css('height', window.innerHeight - 190);
				$('#term_content').scrollspy({offset: 75});
			});
		</script>